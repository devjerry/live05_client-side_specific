
module.exports = {
    entry: "./specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/AppVcc/AppVcc.js",
    output: {
        path: __dirname,
        filename: "./specific_REPO/ThePackagedApp/bundle_app.js"
    },
    module: {
        loaders: [
            { test: /\.css$/i, loader: "style!css" },
            { test: /\.html$/i, loader: "html" }
        ]
    }
};


/*
module.exports = {
    entry: {
        app: "./specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/AppVcc/AppVcc.js",
        home: "./specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/Page4HomeVcc/Page4HomeVcc.js",
        station: "./specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/Page4StationDtlVcc/Page4StationDtlVcc.js"
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].entry.js"
    },
    module: {
        loaders: [
            { test: /\.css$/i, loader: "style!css" },
            { test: /\.html$/i, loader: "html" }
        ]
    }
};
*/
