/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: AppVccConfigFnc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {
	function configFnc(LocationProvider, ResourceProvider, StateProvider, UrlRouterProvider, RouterProvider) {
		var cc = {};
		
		LocationProvider
			.html5Mode(false)
			.hashPrefix('!');

		cc.Router = RouterProvider.getRouter(StateProvider, UrlRouterProvider);
		
		// Don't strip trailing slashes from calculated URLs
		ResourceProvider.defaults.stripTrailingSlashes = false;	
		
		// Execute settting of routes.
		cc.Router.setRoutes();
	}
	
	return configFnc;
})();
