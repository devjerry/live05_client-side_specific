/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: AppVccRouterPrvdrClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {
	

	function RouterPrvdrClass() {
		return this;
	};
	//	Prototype its methods.
	//{	
		var p = RouterPrvdrClass.prototype;
		
		p.getRouter = function getRouterFnc(StateProvider, UrlRouterProvider) {	
			//	Inits: 
			//{
				//	Scope for this script.
				var vv = {};
				vv.methods = {};
			//}
			

			//	Define methods.
			//{	
				vv.methods.setRoutes = function setRoutes_fnc() {
					
					// For any unmatched url, redirect to /state1
					UrlRouterProvider.otherwise('/home');
	
					var ContentHolderWithPlayerAbsState = {
						name: 'ContentHolderWithPlayerAbsState',
						abstract : true,
						views: {
							'BodyContentHolderSlot': {
								template: '<drct-content-holder-with-player-vcc></drct-content-holder-with-player-vcc>'
							}
						}
					};
									
					var OneColStndHolderAbsState = {
						///onEnter: [function(){ alert('Hello from OneColStndHolderAbsState'); }],
						parent: ContentHolderWithPlayerAbsState,
						name: 'OneColStndHolderAbsState',
						abstract : true,
						views: {
							'ContentHolderWithPlayerSlot@ContentHolderWithPlayerAbsState': {
								template: '<drct-one-col-stnd-holder-vcc></drct-one-col-stnd-holder-vcc>'
							}
						}
					};

					var HomePageState = {
						///onEnter: [function(){ alert('Hellow from HomePageState'); }],
						parent: OneColStndHolderAbsState,
						name: 'HomePageState',
						url: '/home',
						views: {
							'OneColStndHolderSlot@OneColStndHolderAbsState': {
								template: '<drct-page-4-home-vcc></drct-page-4-home-vcc>'
							}
						}
					};
					var StationState = {
						parent: OneColStndHolderAbsState,
						name: 'StationState',
						url: '/station{slash1:[/]?}{stationName:[^\/]*}{slash2:[/]?}{stationId:[^\/]*}',
						views: {
							'OneColStndHolderSlot@OneColStndHolderAbsState': {
								template: '<drct-page-4-station-dtl-vcc></drct-station-page-vcc>'
							}
						}
					};

					StateProvider.state(ContentHolderWithPlayerAbsState);
					StateProvider.state(OneColStndHolderAbsState);
					StateProvider.state(HomePageState);
					StateProvider.state(StationState);
				};
			//}
			
			return vv.methods;
		};
			
		p.$get = function getFnc() {};  // Not needed for usage, but needed by Angular.
	//}

	return RouterPrvdrClass;
})();


