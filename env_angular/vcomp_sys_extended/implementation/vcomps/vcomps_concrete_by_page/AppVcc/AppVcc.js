/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: AppVcc.js
 * *********************************************************************
 * *********************************************************************
 */
 module.exports

//	INLINE: 
//{
	//	Scope for this script.
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("../BodyContentHolderVcc/BodyContentHolderVcc.js");
		require("../ContentHolderWithPlayerVcc/ContentHolderWithPlayerVcc.js");
		require("../OneColStndHolderVcc/OneColStndHolderVcc.js");
		///require("../Page4HomeVcc/Page4HomeVcc.js");
		///require("../Page4StationDtlVcc/Page4StationDtlVcc.js");
		
	//}
	
	//	Equated dependencies.
	//{
		aa.configFnc = require("./AppVccConfigFnc.js"); 
		aa.CtrlClass = require("./AppVccCtrlClass.js");
		aa.RouterPrvdrClass = require("./AppVccRouterPrvdrClass.js"); 
	//}

	//	Create module:
	//{
		aa.thisModName = 'AppVcc';
		aa.thisMod = aa.AppVcc = angular.module(aa.thisModName, [
			'ngResource', 
			'ui.router',
			'BodyContentHolderVcc',
			'ContentHolderWithPlayerVcc',
			'OneColStndHolderVcc'
			///'Page4HomeVcc',
			///'Page4StationDtlVcc'
		]);
	//}
	
	//	Set module's config:
	//{	
		aa.thisMod.config( [
			'$locationProvider',
			'$resourceProvider', 
			'$stateProvider', 
			'$urlRouterProvider',
			'AppVccRouterProvider',
			aa.configFnc
		] );
	//}
		
	//	Set module's (only) controller:
	//{	
		aa.ctrlName = 'AppVccVwCtrl';
		createCtrl(aa.thisMod, aa.ctrlName) 
		function createCtrl(module, ctrlName) {
			var vv = {};
			vv.CtrlClass = aa.CtrlClass
			vv.ctrlInjects = ['$scope'];
			vv.CtrlClass.$inject = vv.ctrlInjects;
			module.controller(ctrlName, vv.CtrlClass);
		}	
	//}

	
	//	Set provider of module router:
	//{	
		aa.thisMod.provider( 'AppVccRouter', aa.RouterPrvdrClass, aa );
	//}
	
	
	
	
//}

