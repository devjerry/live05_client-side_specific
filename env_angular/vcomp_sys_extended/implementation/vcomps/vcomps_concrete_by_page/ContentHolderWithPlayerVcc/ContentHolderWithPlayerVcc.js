/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: ContentHolderWithPlayerVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./ContentHolderWithPlayerVccLinkStyle.css");
		require("../PlayerPanelVcc/PlayerPanelVcc.js");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./ContentHolderWithPlayerVccLinkTmplt.html");
		aa.LinkFncClass = require("./ContentHolderWithPlayerVccLinkFncClass.js");
		aa.CtrlClass = require("./ContentHolderWithPlayerVccCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'ContentHolderWithPlayerVcc';
		aa.thisMod = aa.ContentHolderWithPlayerVcc = angular.module(aa.thisModsName, [
			'PlayerPanelVcc'
		]);
		
		aa.drctName = 'drctContentHolderWithPlayerVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

