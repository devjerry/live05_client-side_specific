/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationsListingsVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg, stationService, userService) {
		var that = this;
		that.scopeNg = scopeNg;
		that.stationService = stationService;
		that.userService = userService;
		that.setup()
	}
		
	var p = CtrlClass.prototype;
	
	p.setup = function setup() {
		var that = this;
		that.setupDatas();
	};
	
	p.setupDatas = function setupDatas() {
		var that = this;
		that.scopeNg.datas = {};
		that.scopeNg.datas.control = {};
		that.scopeNg.datas.external = {
			stations: that.getStations(that.scopeNg.atrbListingsType)
		};
		that.scopeNg.datas.view = {
			mech: {
				pleaseWaitDisplayStyle: 'none',
				atrbListingsType: that.scopeNg.atrbListingsType
			},
			info: {
				myname: 'StationsListingsVcc',
				stations: that.scopeNg.datas.external.stations,
				listingsTitle: that.scopeNg.atrbListingsType
			}
		};
		console.log("StationsListings - stations : ", that.scopeNg.datas.view.info.stations);
	};

	p.getStations = function getStations(listingsType) {	
		var that = this;
		var vv = {};
		vv.filters = listingsType;
		vv.userListingsType = "public";
		vv.useMockData = true;
		
		if ( vv.filters === 'topStoriesStations' ) {
			vv.getter =  that.stationService.getListings;
		}
		else {
			vv.getter =  that.userService.getListings;
		}
		vv.datas = vv.getter(
			{
				filters: that.scopeNg.atrbListingsType, 
				userListingsType: "public", 
				useMockData: true
			}
		);
		
		return vv.datas;
	};
	
	
	return CtrlClass;
	
})();




