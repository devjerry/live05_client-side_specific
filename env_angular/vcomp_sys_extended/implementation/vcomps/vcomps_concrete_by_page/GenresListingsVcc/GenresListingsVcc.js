/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: GenresListingsVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 //
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./GenresListingsVccLinkStyle.css");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./GenresListingsVccLinkTmplt.html");
		aa.LinkFncClass = require("./GenresListingsVccLinkFncClass.js");
		aa.CtrlClass = require("./GenresListingsVccCtrlClass.js");
		aa.GenreServiceDef = require("../../../agents/agents_concrete/GenreServiceDef/GenreServiceDef.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'GenresListingsVcc';
		aa.thisMod = aa.GenresListingsVcc = angular.module(aa.thisModsName, [
		]);
		
		aa.drctName = 'drctGenresListingsVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
	
	
	//	Create Genre Service for this module.
	//	...Note since the service definiton is a singleton it will share (as desired)
	//	...certain properties with creations in other modules.:
	//{
		///aa.tryInjectFnc = function() { alert("Hello from tryInjectFnc") };
		aa.thisMod.factory('genreService', ['$window', aa.GenreServiceDef])
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', 'genreService', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {
				atrbListingsType: '@'
			},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

