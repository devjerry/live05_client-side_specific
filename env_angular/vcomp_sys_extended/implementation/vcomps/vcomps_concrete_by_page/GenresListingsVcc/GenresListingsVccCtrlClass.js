/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: GenresListingsVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg, genreService) {
		var that = this;
		that.scopeNg = scopeNg;
		that.genreService = genreService;
		that.setup()
	}
		
	var p = CtrlClass.prototype;
	
	p.setup = function setup() {
		var that = this;
		that.setupDatas();
	};
	
	p.setupDatas = function setupDatas() {
		var that = this;
		that.scopeNg.datas = {};
		that.scopeNg.datas.control = {};
		that.scopeNg.datas.external = {
			genres: that.getGenres()
		};
		that.scopeNg.datas.view = {
			mech: {
				pleaseWaitDisplayStyle: 'none',
				atrbListingsType: that.scopeNg.atrbListingsType
			},
			info: {
				myname: 'GenresListingsVcc',
				genres: that.scopeNg.datas.external.genres,
				listingsTitle: that.scopeNg.atrbListingsType
			}
		};
		console.log("GenresListings - genres : ", that.scopeNg.datas.view.info.genres);
	};

	p.getGenres = function getGenres() {	
		var that = this;
		var vv = {};
		vv.filters = "all";
		vv.userListingsType = "public";
		vv.useMockData = true;

		vv.getter =  that.genreService.getListings;
		vv.datas = vv.getter(
			{
				filters: vv.filters, 
				userListingsType: "public", 
				useMockData: true
			}
		);
		
		return vv.datas;
	};
	
	
	return CtrlClass;
	
})();




