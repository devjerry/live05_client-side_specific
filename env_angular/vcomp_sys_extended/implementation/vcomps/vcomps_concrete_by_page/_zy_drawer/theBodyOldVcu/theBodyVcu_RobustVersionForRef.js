/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: theBodyVcu.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 theBodyVcuCtrlClass
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
	//}
	
	//	Equated dependencies.
	//{
	//}
	
	//	Specified dependencies.
	//{
		aa.requirePathForCtrlClass = "./theBodyVcuCtrlClass.js";
	//}
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'theBodyVcu';
		aa.thisMod = aa.theBodyVcu = angular.module(aa.thisModsName, [
		]);
		
		aa.drctName = 'theBodyVcuDrct';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.template = provideTemplateHtml(); 
			vv.LinkFncClass = provideLinkFncClass(aa.requirePathForCtrlClass);  
			vv.CtrlClass = provideCtrlClass();
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: true,
			restrict: 'E',
			template: vv.template,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	



//	PROVIDERS.
//{
	//	TEMPLATE PROVIDER.

	function provideTemplateHtml(requirePath) {
		if (requirePath) {
			var templateHtml = require(requirePath)
		}
		else {	
			var templateHtml = '<p style="border:solid 1px red; padding: 3px;">Click to scare {{datas.view.info.name}}!</p>'
		}
		return templateHtml;
	};
	aa.provideTemplateHtml = provideTemplateHtml // <- In aa scope for easier access by tester.




	//	LINK FILE CLASS PROVIDER.
	function provideLinkFncClass(requirePath) {
		if (requirePath) {
			var LinkFncClass = require(requirePath)
		}
		else {	
			function LinkFncClass(scopeNg, elm, attr) {
				var that = this;
				that.scopeNg = scopeNg;
				that.elm = elm;
				that.attr = attr;
				that.setup();
			};
				
			var p = LinkFncClass.prototype;
			
			p.setup = function setup() {
				var that = this;
				that.setClickHandler();
			};
			
			p.setClickHandler = function setClickHandler() {
				var that = this;
				that.elm.on( 
					{ 
						'click': function handleClick_fnc(Event) {
							alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
						}
					}
				)
			};
		}
		
		return LinkFncClass;
	};
	aa.provideLinkFncClass = provideLinkFncClass // <- In aa scope for easier access by tester.
	
	
	
	//	CTRL CLASS PROVIDER.
	function provideCtrlClass(requirePath) {
		
		if (requirePath) {
			var Ctrl = require(requirePath)
		}
		else {	
			function Ctrl(scopeNg) {
				var that = this;
				that.scopeNg = scopeNg;
				that.setup()
			}
				
			var p = Ctrl.prototype;
			
			p.setup = function setup() {
				var that = this;
				that.setupDatas();
			};
			
			p.setupDatas = function setupDatas() {
				var that = this;
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {},
						info: {
							name: 'Mary'
						}
					}
				};
			};
		}

		return Ctrl;
	}
	aa.provideCtrlClass = provideCtrlClass // <- In aa scope for easier access by tester.
//}

