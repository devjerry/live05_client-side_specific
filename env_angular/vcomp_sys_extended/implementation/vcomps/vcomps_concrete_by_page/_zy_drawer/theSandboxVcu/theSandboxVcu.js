module.exports


/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: theSandboxVcu.js
 * *********************************************************************
 * *********************************************************************
 */
 
//	INLINE: 
//{
	var aa = {};
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'theSandboxVcu';
		aa.thisMod = aa.theSandboxVcu = angular.module(aa.thisModsName, [
		]);
		
		aa.drctName = 'theSandboxVcuDrct';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		var drctInjects = ['$compile'];
		returnDrctDefinition.$inject = drctInjects;
		module.directive(drctName, returnDrctDefinition);	
		
	}
	
	function returnDrctDefinition($compile) {
		var vv = {}
		vv.drctRawParts = getDrctRawParts();
		vv.template = bgetDrctTemplate(vv.drctRawParts.template);
		vv.link = bgetDrctLinkFnc(vv.drctRawParts.LinkFncClass);
		vv.controller = readyDrctCtrl(vv.drctRawParts.CtrlClass); 
		///vv.controller = ['$scope', vv.drctRawParts.CtrlClass];  This works too.


		
		return {
			scope: true,
			restrict: 'E',
			template: vv.template,
			link: vv.link,
			controller: vv.controller
		};
	};
	
	function getDrctRawParts() {
		/*	Set raw parts of Directive, ether source from providers in this file */
		var parts = {}
		parts.template = provideTemplateHtml();  
		parts.LinkFncClass = provideLinkFncClass();  	
		parts.CtrlClass = provideCtrlClass();
		
		return parts;	
	}
	
	function bgetDrctTemplate(template) {
		/*	Reason for this function is to possibly to modify/etc the param template. */
		return template;
	}		

	function readyDrctCtrl(CtrlClass) {	  	
		var ctrlInjects = ['$scope'];
		CtrlClass.$inject = ctrlInjects;
		return CtrlClass;
	}
	
	function bgetDrctLinkFnc(LinkFncClass) {
		var instanciateLinkFnc = function(scopeNgg, elm, attr) { 
			new LinkFncClass(scopeNgg, elm, attr);
		}
		return instanciateLinkFnc;
	}
//}	



//	PROVIDERS IN LIEU OF A REQUIERERS.
//{
	//	TEMPLATE PROVIDER IN LIEU OF A REQUIER.

	function provideTemplateHtml() {
		var templateHtml = '<p style="border:solid 1px red; padding: 3px;">Click to scare {{datas.view.info.name}}!</p>'
		return templateHtml;
	};
	aa.provideTemplateHtml = provideTemplateHtml // <- In aa scope for easier access by tester.




	//	LINK FILE CLASS PROVIDER IN LIEU OF A REQUIER.
	function provideLinkFncClass() {

		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		return LinkFncClass;
	};
	aa.provideLinkFncClass = provideLinkFncClass // <- In aa scope for easier access by tester.
	
	
	
	//	CTRL CLASS PROVIDER IN LIEU OF A REQUIER.
	function provideCtrlClass() {
			
		function Ctrl(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = Ctrl.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {},
					info: {
						name: 'Sally'
					}
				}
			};
		};

		return Ctrl;
	}
	aa.provideCtrlClass = provideCtrlClass // <- In aa scope for easier access by tester.
//}

