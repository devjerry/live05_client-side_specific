module.exports


/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: theSandboxcVcu.js
 * *********************************************************************
 * *********************************************************************
 */
 	
//	INLINE: 
//{
	var aa = {};
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'theSandboxcVcu';
		aa.thisMod = aa.theSandboxcVcu = angular.module(aa.thisModsName, [
		]);
		
		aa.drctName = 'theSandboxcVcuDrct';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.template = provideTemplateHtml(); 
			vv.LinkFncClass = provideLinkFncClass();  
			vv.CtrlClass = provideCtrlClass();
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: true,
			restrict: 'E',
			template: vv.template,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	


//	PROVIDERS IN LIEU OF A REQUIERERS.
//{
	//	TEMPLATE PROVIDER IN LIEU OF A REQUIER.

	function provideTemplateHtml() {
		var templateHtml = '<p style="border:solid 1px red; padding: 3px;">Click to scare {{datas.view.info.name}}!</p>'
		return templateHtml;
	};
	aa.provideTemplateHtml = provideTemplateHtml // <- In aa scope for easier access by tester.




	//	LINK FILE CLASS PROVIDER IN LIEU OF A REQUIER.
	function provideLinkFncClass() {

		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
			alert("A link fnc made");
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		return LinkFncClass;
	};
	aa.provideLinkFncClass = provideLinkFncClass // <- In aa scope for easier access by tester.
	
	
	
	//	CTRL CLASS PROVIDER IN LIEU OF A REQUIER.
	function provideCtrlClass() {
			
		function Ctrl(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = Ctrl.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
			alert("A drct ctrl made");
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {},
					info: {
						name: 'Bob'
					}
				}
			};
		};

		return Ctrl;
	}
	aa.provideCtrlClass = provideCtrlClass // <- In aa scope for easier access by tester.
//}
