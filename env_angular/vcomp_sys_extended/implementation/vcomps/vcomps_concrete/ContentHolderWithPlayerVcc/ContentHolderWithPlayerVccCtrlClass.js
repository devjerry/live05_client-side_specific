/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: ContentHolderWithPlayerVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg ) {
		scopeNg.thisc = this;
		var that = this;
		that.scopeNg = scopeNg;
		that._setup();
	}
		
	var p = CtrlClass.prototype;
	
	//	================================================================
	//	PUBLIC METHODS:
	//{
		p.switchOnOffPlayer = function switchOnOffFooterOffset( doSwitchOn ) {
			var that = this;
			if ( doSwitchOn ) {
				///that.scopeNg.datas.view.mech.isOkayToShowPlayer = false;
				that.scopeNg.datas.view.mech.isOkayToShowPlayer = true;
			}
			else {
				that.scopeNg.datas.view.mech.isOkayToShowPlayer = false;
				///that.scopeNg.datas.view.mech.isOkayToShowPlayer = true;
			}
			that.scopeNg.$apply();
			console.log(">>> that.scopeNg.datas.view.mech.isOkayToShowPlayer: ", that.scopeNg.datas.view.mech.isOkayToShowPlayer, that);
			
		}
	//}
	
	//	================================================================
	//	PRIVATE METHODS:
	//{	
		p._setup = function setup() {
			var that = this;
			that._setupDatas();
		};
		
		p._setupDatas = function _setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						isOkayToShow: true,
						isOkayToShowPlayer: false
					},
					info: {
						myname: 'ContentHolderWithPlayerVcc',
						time: Date.now()
					}
				}
			};
		};
	//}

	return CtrlClass;
	
})();



