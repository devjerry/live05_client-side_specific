/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: Page4StationDtlVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg, stateNg, stateParamsNg, timeoutNg, mediatorService ) {
		scopeNg.thisc = this;
		var that = this;
		that.scopeNg = scopeNg;
		that.stateNg = stateNg;
		that.stateParamsNg = stateParamsNg;
		that.timeoutNg = timeoutNg;
		that.mediatorService = mediatorService;
		that._setup();
		that._notifyChangedPageState();
	}
		
	var p = CtrlClass.prototype;
	
	//	================================================================
	//	PUBLIC METHODS:
	//{}	
	
	
	//	================================================================
	//	PRIVATE METHODS:
	//{	
		p._setup = function setup() {
			var that = this;
			that._setupDatas();
		};
		
		p._setupDatas = function setupDatas() {
			var that = this;
			console.log( ">>> Page4StationDtlVccCtrlClass - stationId - stationName: ",that.stateParamsNg.stationId, that.stateParamsNg.stationName, that.mediatorService);
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						_atrbStationId: that.stateParamsNg.stationId,
						_atrbStationName: that.stateParamsNg.stationName,
						_atrbUserDisplayName: "Spud Bud "+Date.now()
					},
					info: {
						myname: 'Page4StationDtlVcc'
					}
				}
			};
		};
		
		p._notifyChangedPageState = function _notifyChangedPageState() {
			var that = this;
			var pkg = {
				notice: {
					noticeName: 'Changed_PageState',
					pageStateName: that.stateNg.current.name,
					stationId: that.stateParamsNg.stationId,
					stationName: that.stateParamsNg.stationName
				}
			}
			var asyncNotify = function() {
				that.mediatorService.receiveNotice(that.scopeNg, pkg);
			}
			that.timeoutNg(asyncNotify);
		}


	//}

	return CtrlClass;
	
})();


