/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: Page4StationDtlVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./Page4StationDtlVccLinkStyle.css");
		require("../StationInfoPanelVcc/StationInfoPanelVcc.js");
		require("../ChatPanelVcc/ChatPanelVcc.js");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./Page4StationDtlVccLinkTmplt.html");
		aa.LinkFncClass = require("./Page4StationDtlVccLinkFncClass.js");
		aa.CtrlClass = require("./Page4StationDtlVccCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'Page4StationDtlVcc';
		aa.thisMod = aa.Page4StationDtlVcc = angular.module(aa.thisModsName, [
			'StationInfoPanelVcc',
			'ChatPanelVcc'
		]);
		
		aa.drctName = 'drctPage4StationDtlVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', '$state', '$stateParams', '$timeout', 'mediatorService', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

