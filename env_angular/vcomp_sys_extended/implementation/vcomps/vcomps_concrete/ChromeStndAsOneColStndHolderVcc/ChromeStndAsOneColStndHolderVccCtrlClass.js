/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: ChromeStndAsOneColStndHolderVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg) {
		scopeNg.thisc = this;
		var that = this;
		that.scopeNg = scopeNg;
		that._setup()
	}
		
	var p = CtrlClass.prototype;
	
	//	================================================================
	//	PUBLIC METHODS:
	//{
		p.switchOnOffFooterOffset = function switchOnOffFooterOffset( doSwitchOn ) {
			var that = this;
			if ( doSwitchOn ) {
				that.scopeNg.datas.view.mech.string_footerOffset_yesOrNo = 'footerOffset_yes';
			}
			else {
				that.scopeNg.datas.view.mech.string_footerOffset_yesOrNo = 'footerOffset_no';
			}
			
		}
	//}

	//	================================================================
	//	PRIVATE METHODS:
	//{	
		p._setup = function _setup() {
			var that = this;
			that._setupDatas();
		};
		
		p._setupDatas = function _setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						isOkayToShow: true, 
						string_footerOffset_yesOrNo: 'footerOffset_yes'
					},
					info: {
						myname: 'ChromeStndAsOneColStndHolderVcc',
						time: Date.now()
					}
				}
			};
		};

	return CtrlClass;
	
	//}
	
})();



