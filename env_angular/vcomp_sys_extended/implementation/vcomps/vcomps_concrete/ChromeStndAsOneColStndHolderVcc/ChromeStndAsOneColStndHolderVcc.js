/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: ChromeStndAsOneColStndHolderVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./ChromeStndAsOneColStndHolderVccLinkStyle.css");
		require("../Header4StndUseVcc/Header4StndUseVcc.js");
		require("../HierarchyNavStripVcc/HierarchyNavStripVcc.js");
		require("../Footer4StndUseVcc/Footer4StndUseVcc.js");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./ChromeStndAsOneColStndHolderVccLinkTmplt.html");
		aa.LinkFncClass = require("./ChromeStndAsOneColStndHolderVccLinkFncClass.js");
		aa.CtrlClass = require("./ChromeStndAsOneColStndHolderVccCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'ChromeStndAsOneColStndHolderVcc';
		aa.thisMod = aa.ChromeStndAsOneColStndHolderVcc = angular.module(aa.thisModsName, [
			'Header4StndUseVcc',
			'HierarchyNavStripVcc',
			'Footer4StndUseVcc'
		]);
		
		aa.drctName = 'drctChromeStndAsOneColStndHolderVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

