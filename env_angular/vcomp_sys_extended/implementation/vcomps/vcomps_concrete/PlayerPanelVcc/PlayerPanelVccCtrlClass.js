/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: PlayerPanelVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg ) {
		scopeNg.thisc = this;
		var that = this;
		that.scopeNg = scopeNg;
		that._setup();
	}
		
	var p = CtrlClass.prototype;
	
	//	================================================================
	//	PUBLIC METHODS:
	//{
		p.setStation = function setStation(stationId, doPlayFlag) {
			var that = this;
			that.scopeNg.datas.view.info.stationId = stationId;
			that.scopeNg.datas.view.info.doPlayFlag = doPlayFlag;  	// Valid options are 0 or 1.
			that.scopeNg.datas.view.info.iframeSrc = that._bgetIframeSrc();
			that.scopeNg.$apply();
		}
		
		p.getStationId = function getStationId() {
			var that = this;
			return that.scopeNg.datas.view.info.stationId;
		}
	//}
	
	//	================================================================
	//	PRIVATE METHODS:
	//{	
		p._setup = function setup() {
			var that = this;
			that._setupDatas();
		};
		
		p._setupDatas = function setupDatas() {
			var that = this;
			that.mech = {};
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						isOkayToShow: true

					},
					info: {
						myname: 'PlayerPanelVcc',
						stationId: "",
						doPlayFlag: 0,		// Valid options are 0 or 1.
						iframeSrc: ""
					}
				}
			};

			///that.scopeNg.datas.view.info.iframeSrc  = that._bgetIframeSrc();
		};
		
		p._bgetIframeSrc = function getIframeSrc() {
			var that = this;
			var vv = {};
			
			///return "http://pi-web01.nanocosm.com/?station_id=7448"
			console.log(">>> that.scopeNg.datas.view.info.stationId: ", that.scopeNg.datas.view.info.stationId);
			
			if ( that.scopeNg.datas.view.info.stationId ) {
				vv.newSrcUrl = "http://pi-web01.nanocosm.com/?station_id="+that.scopeNg.datas.view.info.stationId;
			}
			else {
				vv.newSrcUrl = that.mech.currentStationId || "http://pi-web01.nanocosm.com/?station_id=7448";
			}
			
			if (that.scopeNg.datas.view.info.doPlayFlag === 1) {
				vv.newSrcUrl = vv.newSrcUrl + "&play=" + 1;
			}
			
			
			console.log(">>> In that.scopeNg.methods.getIframeSrc - vv.newSrcUrl: ", vv.newSrcUrl) 
			
			that.mech.currentStationId = vv.newSrcUrl;
			return vv.newSrcUrl;

		};
	//}


	return CtrlClass;
	
})();
