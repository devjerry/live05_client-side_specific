/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: HierarchyNavStripVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg, stateNg, stateParamsNg) {
		scopeNg.thisc = this;
		var that = this;
		that.scopeNg = scopeNg;
		that.stateNg = stateNg;
		that.stateParamsNg = stateParamsNg;
		that._setup()
		that._setViewDataPerState();
	}
		
	var p = CtrlClass.prototype;
	
	//	================================================================
	//	PUBLIC METHODS:
		p.setDisplay = function setDisplay() {
			var that = this;
			that._setViewDataPerState();
		}
	//{}	
	
	
	//	================================================================
	//	PRIVATE METHODS:
	//{	
		p._setup = function _setup() {
			var that = this;
			that._setupDatas();
		};
		
		p._setupDatas = function _setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'HierarchyNavStripVcc'
					}
				}
			};
		};
		
		p._setViewDataPerState = function _setViewDataPerState() {
			var that = this;
			var vv = {};
			vv.currentStateName = that.stateNg.current.name;
			
			vv.statesViewData = that._bgetStatesViewData();
			vv.stateViewData = vv.statesViewData[vv.currentStateName];
			if (vv.stateViewData) {
				that.scopeNg.datas.view.mech.isOkayToShow = vv.stateViewData.isOkayToShow;
				that.scopeNg.datas.view.info.navThisPageName = vv.stateViewData.navThisPageName;
			}

		};
		
		p._bgetStatesViewData = function _bgetStatesViewData() {
			var that = this;
			var vv = {};
			
			vv.statesViewData = {
				Page4HomeState: {
					navThisPageName: "home",
					isOkayToShow: false 
				},
				Page4StationDtlState: {
					navThisPageName: "station info",
					isOkayToShow: true
				}
				
			};
			
			return vv.statesViewData;
		};
		
	//}

	return CtrlClass;
	
})();



