/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationInfoPanelVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./StationInfoPanelVccLinkStyle.css");
		require("../SocialingPanelVcc/SocialingPanelVcc.js");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./StationInfoPanelVccLinkTmplt.html");
		aa.LinkFncClass = require("./StationInfoPanelVccLinkFncClass.js");
		aa.CtrlClass = require("./StationInfoPanelVccCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'StationInfoPanelVcc';
		aa.thisMod = aa.StationInfoPanelVcc = angular.module(aa.thisModsName, [
			'SocialingPanelVcc',
		]);
		
		aa.drctName = 'drctStationInfoPanelVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', 'stationsService', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {
				atrbStationId: '@',
				atrbStationName: '@'
			},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

