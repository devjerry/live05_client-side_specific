/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationInfoPanelVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg, stationsService) {
		var that = this;
		that.scopeNg = scopeNg;
		that.stationsService = stationsService;
		that.setup()
	}
		
	var p = CtrlClass.prototype;
	
	p.setup = function setup() {
		var that = this;
		that.setupDatas();
	};
	
	p.setupDatas = function setupDatas() {
		var that = this;
		that.scopeNg.datas = {};
		that.scopeNg.datas.control = {};
		that.scopeNg.datas.external = {}
		that.scopeNg.datas.external.station = {};
		
		that.scopeNg.datas.external.station = that.getStation(that.scopeNg.atrbStationId);
		console.log(">>> that.scopeNg.atrbStationId ", that.scopeNg.atrbStationId);
		console.log(">>> StationInfoPanelVccCtrlClass - station (a): ", that.scopeNg.datas.external.station);

		that.scopeNg.datas.control = {};
		that.scopeNg.datas.view = {
			mech: {
				pleaseWaitDisplayStyle: 'none'
			},
			info: {
				myname: 'StationInfoPanelVcc',
				station: that.scopeNg.datas.external.station
			}
		};
		///console.log(">>> StationInfoPanelVccCtrlClass - station (b): ", that.scopeNg.datas.external.station);
	};
	
	p.getStation = function getStation(id) {	
		var that = this;
		var vv = {};
		vv.data = that.stationsService.getStation(id);

		return vv.data;
	};

	return CtrlClass;
	
})();



