/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: AppVccRouterPrvdrClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {
	

	function RouterPrvdrClass() {
		return this;
	};
	//	Prototype its methods.
	//{	
		var p = RouterPrvdrClass.prototype;
		
		p.getRouter = function getRouterFnc(StateProvider, UrlRouterProvider) {	
			//	Inits: 
			//{
				//	Scope for this script.
				var vv = {};
				vv.methods = {};
			//}
			

			//	Define methods.
			//{	
				vv.methods.setRoutes = function setRoutes_fnc() {
					
					// For any unmatched url, redirect to /state1
					UrlRouterProvider.otherwise('/home');
	
					var ContentHolderWithPlayerAbsState = {
						name: 'ContentHolderWithPlayerAbsState',
						abstract : true,
						views: {
							'BodyContentHolderSlot': {
								template: '<drct-content-holder-with-player-vcc></drct-content-holder-with-player-vcc>'
							}
						}
					};
									
					var ChromeStndAsOneColStndHolderAbsState = {
						///onEnter: [function(){ alert('Hello from ChromeStndAsOneColStndHolderAbsState'); }],
						parent: ContentHolderWithPlayerAbsState,
						name: 'ChromeStndAsOneColStndHolderAbsState',
						abstract : true,
						views: {
							'ContentHolderByPlayerSlot@ContentHolderWithPlayerAbsState': {
								template: '<drct-chrome-stnd-as-one-col-stnd-holder-vcc></drct-chrome-stnd-as-one-col-stnd-holder-vcc>'
							}
						}
					};

					var Page4HomeState = {
						///onEnter: [function(){ alert('Hellow from HomePageState'); }],
						parent: ChromeStndAsOneColStndHolderAbsState,
						name: 'Page4HomeState',
						url: '/home',
						views: {
							'ChromeStndAsOneColStndHolderSlot@ChromeStndAsOneColStndHolderAbsState': {
								template: '<drct-page-4-home-vcc></drct-page-4-home-vcc>'
							}
						}
					};
					var Page4StationDtlState = {
						parent: ChromeStndAsOneColStndHolderAbsState,
						name: 'Page4StationDtlState',
						url: '/station{slash1:[/]?}{stationName:[^\/]*}{slash2:[/]?}{stationId:[^\/]*}',
						views: {
							'ChromeStndAsOneColStndHolderSlot@ChromeStndAsOneColStndHolderAbsState': {
								template: '<drct-page-4-station-dtl-vcc></drct-page-4-station-dtl-vcc>'
							}
						}
					};

					StateProvider.state(ContentHolderWithPlayerAbsState);
					StateProvider.state(ChromeStndAsOneColStndHolderAbsState);
					StateProvider.state(Page4HomeState);
					StateProvider.state(Page4StationDtlState);
				};
			//}
			
			return vv.methods;
		};
			
		p.$get = function getFnc() {};  // Not needed for usage, but needed by Angular.
	//}

	return RouterPrvdrClass;
})();


