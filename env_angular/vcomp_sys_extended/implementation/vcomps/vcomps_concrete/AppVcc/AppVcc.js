/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: AppVcc.js
 * *********************************************************************
 * *********************************************************************
 */
 module.exports

//	INLINE: 
//{
	//	Scope for this script.
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("../BodyContentHolderVcc/BodyContentHolderVcc.js");
		require("../ContentHolderWithPlayerVcc/ContentHolderWithPlayerVcc.js");
		require("../ChromeStndAsOneColStndHolderVcc/ChromeStndAsOneColStndHolderVcc.js");
		require("../Page4HomeVcc/Page4HomeVcc.js");
		require("../Page4StationDtlVcc/Page4StationDtlVcc.js");
		require("../../../action_directives/actdrct_concrete/ClickToMediatorAdc/ClickToMediatorAdc.js");
	//}
	
	//	Equated dependencies.
	//{
		aa.configFnc = require("./AppVccConfigFnc.js"); 
		aa.CtrlClass = require("./AppVccCtrlClass.js");
		aa.RouterPrvdrClass = require("./AppVccRouterPrvdrClass.js"); 
		aa.MediatorServiceClass = require("../../../agents/agents_concrete/MediatorServiceClass.js");
		aa.RemoteDatasServiceClass = require("../../../agents/agents_concrete/RemoteDatasServiceClass.js");
		aa.StationsServiceClass = require("../../../agents/agents_concrete/StationsServiceClass.js");
		aa.GenresServiceClass = require("../../../agents/agents_concrete/GenresServiceClass.js");
		aa.UsersServiceClass = require("../../../agents/agents_concrete/UsersServiceClass.js");
		
	//}

	//	Create module:
	//{
		aa.thisModName = 'AppVcc';
		aa.thisMod = aa.AppVcc = angular.module(aa.thisModName, [
			'ngResource', 
			'ui.router',
			'BodyContentHolderVcc',
			'ContentHolderWithPlayerVcc',
			'ChromeStndAsOneColStndHolderVcc',
			'Page4HomeVcc',
			'Page4StationDtlVcc',
			'ClickToMediatorAdc',
			'slick'
		]);
	//}
	
	//	Set module's config:
	//{	
		aa.thisMod.config( [
			'$locationProvider',
			'$resourceProvider',
			'$sceDelegateProvider', 
			'$stateProvider', 
			'$urlRouterProvider',
			'AppVccRouterProvider',
			aa.configFnc
		] );
	//}
	
	//	Set modules (app's) services:
	//{	
		aa.thisMod.service('mediatorService', ['$timeout', aa.MediatorServiceClass]);
		aa.thisMod.service('remoteDatasService', ['$resource', aa.RemoteDatasServiceClass]);
		aa.thisMod.service('stationsService', ['$q', '$resource', aa.StationsServiceClass]);
		aa.thisMod.service('genresService', ['$q', '$resource', aa.GenresServiceClass]);
		aa.thisMod.service('usersService', ['$q', '$resource', aa.UsersServiceClass]);
	//}
		
	//	Set module's (only) controller:
	//{	
		aa.ctrlName = 'AppVccVwCtrl';
		createCtrl(aa.thisMod, aa.ctrlName) 
		function createCtrl(module, ctrlName) {
			var vv = {};
			vv.CtrlClass = aa.CtrlClass
			vv.ctrlInjects = ['$scope'];
			vv.CtrlClass.$inject = vv.ctrlInjects;
			module.controller(ctrlName, vv.CtrlClass);
		}	
	//}

	
	//	Set provider of module router:
	//{	
		aa.thisMod.provider( 'AppVccRouter', aa.RouterPrvdrClass, aa );
	//}
	
//}

