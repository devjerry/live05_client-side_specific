/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: Page4HomeVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./Page4HomeVccLinkStyle.css");
		require("../MastheadPanelVcc/MastheadPanelVcc.js");
		require("../GenresPanelVcc/GenresPanelVcc.js");
		require("../StationsPanelVcc/StationsPanelVcc.js");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./Page4HomeVccLinkTmplt.html");
		aa.LinkFncClass = require("./Page4HomeVccLinkFncClass.js");
		aa.CtrlClass = require("./Page4HomeVccCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'Page4HomeVcc';
		aa.thisMod = aa.Page4HomeVcc = angular.module(aa.thisModsName, [
			'MastheadPanelVcc',
			'GenresPanelVcc',
			'StationsPanelVcc'
		]);
		
		aa.drctName = 'drctPage4HomeVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', '$state', '$stateParams', '$timeout', 'mediatorService', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

