/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationsListingsVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 //
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./StationsListingsVccLinkStyle.css");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./StationsListingsVccLinkTmplt.html");
		aa.LinkFncClass = require("./StationsListingsVccLinkFncClass.js");
		aa.CtrlClass = require("./StationsListingsVccCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'StationsListingsVcc';
		aa.thisMod = aa.StationsListingsVcc = angular.module(aa.thisModsName, [
		]);
		
		aa.drctName = 'drctStationsListingsVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
	
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', '$timeout', 'stationsService', 'usersService', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {
				atrbListingsType: '@',
				atrbLayoutType: '@'
			},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

