/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationsListingsVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg, timeoutNg, stationsService, usersService) {
		scopeNg.thisc = this;
		var that = this;
		that.scopeNg = scopeNg;
		that.timeoutNg = timeoutNg;
		that.stationsService = stationsService;
		that.usersService = usersService;
		that._setup()
	}
		
	var p = CtrlClass.prototype;
	
	
	//	================================================================
	//	PUBLIC METHODS:
	//{}	
	
	
	//	================================================================
	//	PRIVATE METHODS:
	//{	
		p._setup = function _setup() {
			var that = this;
			that._setupDatas();
			that.testUser01Id = 777;
			that.scopeNg.getStations = that.getStations;
		};
		
		p._setPublicMethods = function _setPublicMethods() {
			var that = this;
			that.scopeNg.getStations = that.getStations;
		}
		
		p._setupDatas = function _setupDatas() {
			var that = this;
			that.scopeNg.datas = {};
			that.scopeNg.datas.control = {};
			that.scopeNg.datas.external = {}
			
			that.scopeNg.datas.external.stations = "";
			
		
			
			that.scopeNg.datas.external.stations = that._getStations(that.scopeNg.atrbListingsType)
			
			that.scopeNg.datas.view = {
				mech: {
					pleaseWaitDisplayStyle: 'none',
					atrbListingsType: that.scopeNg.atrbListingsType,
					doHaveListings: false
				},
				info: {
					myname: 'StationsListingsVcc',
					stations: that.scopeNg.datas.external.stations,
					listingsTitle: that.scopeNg.atrbListingsType
				}
			};
			console.log("StationsListings - stations : ", that.scopeNg.datas.view.info.stations);
		
		
			//  This was added because carousel shouldn't be painted until listing
			//	...resource promise resolves, else carousel won't know how to lay out.
			that.scopeNg.$watch('datas.external.stations', function(newVal, oldVal){	
				if ( that.scopeNg.datas.external.stations  &&  that.scopeNg.datas.external.stations[0] ) {
					that.scopeNg.datas.view.mech.isOkayToShow = true; 
				}
			}, true);

		};

		p._getStations = function _getStations(listingsType) {	
			var that = this;
			var vv = {};
			vv.filters = listingsType;
			vv.userListingsType = "public";
			vv.useMockData = true;
			vv.mockUserId = 777;
			
			if ( vv.filters === 'topStations' ) {
				vv.datas = that.stationsService.getTopStations();
			}
			else if  ( vv.filters === 'favoriteStationsFromIdsListOfUser' ) {
				
				vv.datas = that.usersService.getFavoriteStations(vv.mockUserId);
			}
			else if  ( vv.filters === 'recentStationsFromIdsListOfUser' ) {
				vv.datas = that.usersService.getRecentStations(vv.mockUserId);
			}

			return vv.datas;
		};
	//}
	
	return CtrlClass;
	
})();

