/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: GenresPanelVccCtrlClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function CtrlClass(scopeNg, genreService) {
		var that = this;
		that.scopeNg = scopeNg;
		that.genreService = genreService;
		that.setup()
	}
		
	var p = CtrlClass.prototype;
	
	p.setup = function setup() {
		var that = this;
		that.setupDatas();
	};
	
	p.setupDatas = function setupDatas() {
		var that = this;
		that.scopeNg.datas = {};
		that.scopeNg.datas.control = {};
		that.scopeNg.datas.external = {
			genres: that.genreService.getGenres(
				{
					filters: "some", 
					userType: "public", 
					useMockData: true
				}
			)
		};
		that.scopeNg.datas.view = {
			mech: {
				pleaseWaitDisplayStyle: 'none'
			},
			info: {
				myname: 'GenresPanelVcc',
				genres: that.scopeNg.datas.external.genres
			}
		};
		console.log("GenresPanel - genres : ", that.scopeNg.datas.view.info.genres);
	};

	p.getGenres = function getGenres() {	
	};
	
	
	return CtrlClass;
	
})();




