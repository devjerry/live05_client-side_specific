/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: theBodyVcu.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
	//}
	
	//	Equated dependencies.
	//{
		aa.template = require("./theBodyVcuLinkTemplate.html");
		aa.LinkFncClass = require("./theBodyVcuLinkFncClass.js");
		aa.CtrlClass = require("./theBodyVcuCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'theBodyVcu';
		aa.thisMod = aa.theBodyVcu = angular.module(aa.thisModsName, [
		]);
		
		aa.drctName = 'theBodyVcuDrct';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.template = aa.template; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: true,
			restrict: 'E',
			template: vv.template,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

