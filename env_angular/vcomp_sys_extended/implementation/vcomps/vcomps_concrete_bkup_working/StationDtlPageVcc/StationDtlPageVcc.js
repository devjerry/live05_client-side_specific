/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationDtlPageVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./StationDtlPageVccLinkStyle.css");
		require("../HierarchyNavStripVcc/HierarchyNavStripVcc.js");
		require("../StationInfoPanelVcc/StationInfoPanelVcc.js");
		require("../DjProfilePanelVcc/DjProfilePanelVcc.js");
		require("../LastPlayedPanelVcc/LastPlayedPanelVcc.js");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./StationDtlPageVccLinkTmplt.html");
		aa.LinkFncClass = require("./StationDtlPageVccLinkFncClass.js");
		aa.CtrlClass = require("./StationDtlPageVccCtrlClass.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'StationDtlPageVcc';
		aa.thisMod = aa.StationDtlPageVcc = angular.module(aa.thisModsName, [
			'HierarchyNavStripVcc',
			'StationInfoPanelVcc',
			'DjProfilePanelVcc',
			'LastPlayedPanelVcc'
		]);
		
		aa.drctName = 'drctStationDtlPageVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

