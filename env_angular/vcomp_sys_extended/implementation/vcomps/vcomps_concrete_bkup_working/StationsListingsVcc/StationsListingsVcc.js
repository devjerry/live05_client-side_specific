/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationsListingsVcc.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports
 
 //
//	INLINE: 
//{
	var aa = {};
	
	//	Non-equated dependencies.
	//{
		require("./StationsListingsVccLinkStyle.css");
	//}
	
	//	Equated dependencies.
	//{
		aa.tmplt = require("./StationsListingsVccLinkTmplt.html");
		aa.LinkFncClass = require("./StationsListingsVccLinkFncClass.js");
		aa.CtrlClass = require("./StationsListingsVccCtrlClass.js");
		aa.StationsServiceDef = require("../../../agents/agents_concrete/StationsServiceDef/StationsServiceDef.js");
		aa.UserServiceDef = require("../../../agents/agents_concrete/UserServiceDef/UserServiceDef.js");
	//}
	
	
	//	Create module and its directive:
	//{
		aa.thisModsName = 'StationsListingsVcc';
		aa.thisMod = aa.StationsListingsVcc = angular.module(aa.thisModsName, [
		]);
		
		aa.drctName = 'drctStationsListingsVcc';
		createDrct(aa.thisMod, aa.drctName);
	//}
	
	
	//	Create services for this module.
	//	...Note since the service definiton is a singleton it will share (as desired)
	//	...certain properties with creations in other modules.:
	//{
		aa.thisMod.factory('stationsService', ['$window', aa.StationsServiceDef]);
		aa.thisMod.factory('userService', ['$window', aa.UserServiceDef]);
	//}
	
	//	Create User Service for this module.
	//	...Note since the service definiton is a singleton it will share (as desired)
	//	...certain properties with creations in other modules.:
	//{
		aa.thisMod.factory('userService', ['$window', aa.UserServiceDef])
	//}
//}


//	HELPERS TO BUILD MODULE'S DIRECTIVE:
//{

	function createDrct(module, drctName) {
		module.directive(drctName, ['$compile', drctFunction]);	
	}
	
	function drctFunction($compile) {
		var vv = {}
		
		// Get parts.
		//{
			vv.tmplt = aa.tmplt; 
			vv.LinkFncClass = aa.LinkFncClass;
			vv.CtrlClass = aa.CtrlClass;
		//}
		
		//	Setup the linkFnc.
		vv.linkFnc = function(scopeNgg, elm, attr) { 
			new vv.LinkFncClass(scopeNgg, elm, attr);
		}
		//	Define injection into the Ctrl class.
		vv.controller = ['$scope', 'stationsService',  'userService', vv.CtrlClass];
		
		//	Return the directive's definition.
		return  {
			scope: {
				atrbListingsType: '@'
			},
			restrict: 'E',
			template: vv.tmplt,
			link: vv.linkFnc,
			controller: vv.controller
		};
	};	
//}	

