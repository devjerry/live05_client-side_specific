/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: ClickToMediatorAdcLinkFncClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {
 
	function LinkFncClass(scopeNg, elm, attr, mediatorService) {
		var that = this;
		that.scopeNg = scopeNg;
		that.elm = elm;
		that.attr = attr;
		that.mediatorService = mediatorService;
		that._setup();
		console.log("boooooo3: ");
	};
		
	var p = LinkFncClass.prototype;
	
	p._setup = function _setup() {
		var that = this;
		that._setClickHandler();
	};
	
	
	p._setClickHandler = function _setClickHandler() {
		var that = this;
		
		that.elm.on( 
			{ 
				'click': function handleClick_fnc(evnt) {
					var vv = {};
					vv.pkgForMediator = angular.fromJson(that.attr.drctClickToMediatorAdc);
					vv.pkgForMediator.evnt = evnt;
					//	Send package to mediator
					//{	
						console.log(">>> handleClick_fnc - vv.pkgForMediator: ", vv.pkgForMediator, that);
						that.mediatorService.receiveNotice("", vv.pkgForMediator);
					//}
				}
			}
		)
	};

	return LinkFncClass;
	
})();

