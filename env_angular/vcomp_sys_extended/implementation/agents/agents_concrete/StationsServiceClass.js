/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: StationsServiceClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function ClassConstructor(qNg, resourceNg) {
		var that = this;
		that.qNg = qNg;
		that.resourceNg = resourceNg;
		var breakpoint = 'a';
	}

	var p = ClassConstructor.prototype;

	//	================================================================
	//	PRIVATE METHODS:
	//{
		p._bgetMainResource = _bgetMainResource;
		function _bgetMainResource() {
			var that = this;

			if ( ! that.MainResource ) { 
				that.MainResource = that.resourceNg(
					///	ALT: 'http://pi-node.nanocosm.com:3000/metadata/:modelType/:id?:q1Name:q1Val',
					///'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:modelType/:id?:q1Name:q1Val', 
					'http://pi-api.nanocosm.com:8780/apis/:modelType/:modelSubtype/:id?:q1Name:q1Val', 
					{
						modelType:'stations',
						modelSubtype:'@modelSubtype',
						id:'@id',
						q1Name:'@q1Name',
						q1Val:'@q1Val'
					},
					{
						getListings: { 
							cache: true, 
							method: 'get', 
							isArray: true,
							params: {modelSubtype: 'active'}
						},
						getTopStoriesStations: { 
							cache: true, 
							method: 'get', 
							isArray: true,
							params: {modelSubtype: 'active', q1Name: 'filter=', q1Val: 'topStories'},
						},
						
						getTopStations: { 
							cache: true, 
							method: 'get', 
							isArray: true,
							params: {modelSubtype: 'active', q1Name: 'filter=', q1Val: 'top'},
						},

						getStation: { 
							cache: true, 
							method: 'get', 
							isArray: false
						}
					}
				);
			}

			return that.MainResource;
		}
	//}
	
	//	================================================================
	//	PUBLIC METHODS:
	//{
		
		p.getListings = getListings;
		function getListings() {
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getListings();

			return vv.datas
		}
		
		p.getTopStoriesStations = getTopStoriesStations;
		function getTopStoriesStations() {
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getTopStoriesStations();

			return vv.datas
		}
		
		p.getTopStations = getTopStations;
		function getTopStations() {
			console.log(">>> In getTopStations");
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getTopStations();

			return vv.datas
		}
		
		p.getStation = getStation;
		function getStation(id) {
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getStation({id: id});

			return vv.datas
		}
	//}

	return ClassConstructor;
})();

