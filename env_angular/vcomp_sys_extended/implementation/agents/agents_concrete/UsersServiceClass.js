/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: UsersServiceClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function ClassConstructor(qNg, resourceNg) {
		var that = this;
		that.qNg = qNg;
		that.resourceNg = resourceNg;
		var breakpoint = 'a';
	}

	var p = ClassConstructor.prototype;

	//	================================================================
	//	PRIVATE METHODS:
	//{
		p._bgetMainResource = _bgetMainResource;
		function _bgetMainResource() {
			var that = this;

			if ( ! that.MainResource ) { 
				that.MainResource = that.resourceNg(
					/// ALT: 'http://pi-node.nanocosm.com:3000/metadata/:modelType/:id',
					'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:modelType/:id/?:q1Name:q1Val', 
					{
						modelType:'users',
						id:'@id',
						q1Name:'@q1Name',
						q1Val:'@q1Val'
					},
					{
						getListings: { 
							cache: true, 
							method: 'get', 
							isArray: true
						},
						getItem: { 
							cache: true, 
							method: 'get', 
							isArray: false
						},
						getFavoriteStations: { 
							cache: true, 
							method: 'get', 
							isArray: true,
							params: {q1Name: 'filter=', q1Val: 'favoriteStations'}
						},
						getRecentStations: { 
							cache: true, 
							method: 'get', 
							isArray: true,
							params: {q1Name: 'filter=', q1Val: 'recentStations'},
						}
					}
				);
			}

			return that.MainResource;
		}
	//}
	
	//	================================================================
	//	PUBLIC METHODS:
	//{
		
		p.getListings = getListings;
		function getListings() {
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getListings();
			return vv.datas
		}
		
		p.getMainItem = getMainItem;
		function getMainItem(id) {
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getItem( { id: id } );
			return vv.datas
		}
		
		
		p.getFavoriteStations = getFavoriteStations;
		function getFavoriteStations(id) {
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getFavoriteStations( { id: id } );
			return vv.datas
		}
		

		p.getRecentStations = getRecentStations;
		function getRecentStations(id) {
			var that = this;
			var vv = {}
			vv.MainResource = that._bgetMainResource();
			vv.datas = vv.MainResource.getRecentStations( { id: id } );
			return vv.datas
		}
		

	//}

	return ClassConstructor;
})();


