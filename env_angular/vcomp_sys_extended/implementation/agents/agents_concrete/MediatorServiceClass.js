/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: MediatorServiceClass.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

	function ClassConstructor(timeoutNg) {
		var that = this;
		that.timeoutNg = timeoutNg;
		that._setup();
		var breakpoint = 'a';
	}

	var p = ClassConstructor.prototype;


	//	================================================================
	//	PUBLIC METHODS:
	//{	
		p.receiveNotice = receiveNotice;
		function receiveNotice(senderScope, pkg) {
			var that = this;
			var vv = {};
			vv.handleNoticeReturn = that._handleNotice(senderScope,pkg);
			return vv.handleNoticeReturn;
		}
	//}
	
	
	
	//	================================================================
	//	PRIVATE METHODS:
	//{
	
		p._setup = _setup;
		function _setup() {
		}

		p._handleNotice = _handleNotice;
		function _handleNotice(senderScope, pkg) {
			var that = this;
			var vv = {};
			console.log(">>> _handleNotice: ", senderScope, pkg.notice.noticeName, pkg);
			vv.noticeName = pkg.notice.noticeName;	
			if (that["_run__" + vv.noticeName + "__Xnt"] ) {
				vv.noticeTransactionName = "_run__" + vv.noticeName + "__Xnt";
			}
			else if (that["_run__" + vv.noticeName + "__Nnt"] ) {
				vv.noticeTransactionName = "_run__" + vv.noticeName + "__Nnt";
			}
			
			vv.transReturn = that[vv.noticeTransactionName](senderScope, pkg);
			
			return vv.transReturn
		}


		p._execute = _execute;
		function _execute(targetSelector, targetMethodName, methodArgs) {
			var that = this;
			var vv = {};	
			console.log(">>> mediator._execute: ", targetSelector, targetMethodName, methodArgs);
			if ( angular.element( document.querySelector( targetSelector ) ).scope() ) {
				vv.thisc = angular.element( document.querySelector( targetSelector ) ).scope().thisc;
				vv.method = vv.thisc[targetMethodName];
				vv.methodReturn = vv.method.apply(vv.thisc, methodArgs);
			}
			else {
				console.log(">>> COULD NOT EXECUTE.");
			}
			return vv.methodReturn;
		}
	//}
	
	
	//	================================================================
	//	E(X)TERNAL Notice Transactions: (Xnt)  
	//	...These are for notices that originate *outide* of mediator.
	//{
		p._run__Changed_PageState__Xnt = function(senderScope, pkg) {
			console.log(">>> START: _run__Changed_PageState__Xnt");
			var that = this;
			var vv = {}
			
			//{	Needed params: 
				vv.pageStateName = pkg.notice.pageStateName;
			//}
			
			//	Set display on Hierarchy nav strip.
			that._setDisplayOfHierarchyNavStrip__Rtn();
			
			//	Notify what page changed to in order to delegate to
			//	... the transaction per that specific page type.
			pkg.notice.noticeName = "ChangedTo_" + pkg.notice.pageStateName;
			vv.handleNoticeReturn = that.receiveNotice(senderScope, pkg);
			
			//	Manager Footer Spacing
			that._autoManagePlayerAndFooterDisplay__Rtn();

			console.log(">>> END: _run__Changed_PageState__Xnt");
			return vv.rtnReturn;
		}
		
		p._run__Clicked_StationPlaySwitch__Xnt = function(senderScope, pkg) {
			console.log(">>> START: _run__Clicked_StationPlaySwitch__Xnt");
			var that = this;
			var vv = {}
			
			//{	Needed params: 
				vv.stationId = pkg.notice.stationId;
				vv.doPlayFlag = pkg.notice.doPlayFlag || 1;  //  Values are 1 & 0.
			//}
			
			
			//	Set station on player per station for this page.
			vv.rtnReturn = that._setStationOnPlayer__Rtn(vv.stationId, vv.doPlayFlag)
			
			console.log(">>> END: _run__Clicked_StationPlaySwitch__Xnt");
			return vv.rtnReturn;
		}
	//}
	

	//	================================================================
	//	I(N)TERNAL Notice Transactions: (Nnt)  
	//	...These are for notices that originate *inside* of mediator.
	//{
		p._run__ChangedTo_Page4HomeState__Nnt = function(senderScope, pkg) {
			console.log(">>> START: _run__ChangedTo_Page4HomeState__Nnt");
			var that = this;
			var vv = {}
			
			//	LOGIC TBD.
			console.log(">>> END: _run__ChangedTo_Page4HomeState__Nnt");
		}
		
		p._run__ChangedTo_Page4StationDtlState__Nnt = function(senderScope, pkg) {
			console.log(">>> START: _run__ChangedTo_Page4StationDtlState__Nnt");
			var that = this;
			var vv = {}
			
			//{	Needed params: 
				vv.stationId = pkg.notice.stationId;
				vv.stationName = pkg.notice.stationName;
			//}
			
			//	Set station on player per station for this page.
			///vv.rtnReturn = that._setStationOnPlayer__Rtn(vv.stationId)
			
			console.log(">>> END: _run__ChangedTo_Page4StationDtlState__Nnt");
			///return vv.rtnReturn;
		}
	//}
	
	
	
	//	================================================================
	//	ROUTINES: (Rtn)   
	//	...These are basically "partial transactions", and/or 
	//	...simple transactions around executing just one method.
	//{
		
		p._setStationOnPlayer__Rtn = function(stationId, doPlayFlag) {
			var that = this;
			var vv = {}
			console.log(">>> _setStationOnPlayer__Rtn: ", stationId)
			vv.exeReturn = that._execute.apply(that, 
				[ '.ChromeStndAsOneColStndHolderVcc', 'switchOnOffFooterOffset', [ true ] ]);		
			vv.exeReturn = that._execute.apply(that, 
				[ '.ContentHolderWithPlayerVcc', 'switchOnOffPlayer', [ true ] ]);
			vv.exeReturn = that._execute.apply(that, 
				[ '.PlayerPanelVcc', 'setStation', [stationId, doPlayFlag] ]);

			return vv.exeReturn;
		}
		
		p._setDisplayOfHierarchyNavStrip__Rtn = function() {
			var that = this;
			var vv = {}
			vv.exeReturn = that._execute.apply(that, 
				[ '.HierarchyNavStripVcc', 'setDisplay', [] ])

			return vv.exeReturn;
		}
		
		p._autoManagePlayerAndFooterDisplay__Rtn = function() {
			var that = this;
			var vv = {}

			//	Query player panel controller to see if display switch is on or off.
			vv.doesPlayerHaveStation = that._execute.apply(that, 
				[ '.PlayerPanelVcc', 'getStationId', [] ])	
			
			
			//	If player display switch is on, then instruct for gap under footer.
			//ContentHolderWithPlayerVcc
			if ( vv.doesPlayerHaveStation ) {
				vv.exeReturn = that._execute.apply(that, 
					[ '.ChromeStndAsOneColStndHolderVcc', 'switchOnOffFooterOffset', [ true ] ]);		
				vv.exeReturn = that._execute.apply(that, 
					[ '.ContentHolderWithPlayerVcc', 'switchOnOffPlayer', [ true ] ]);
			}
			//	Else no gap under footer (default state)
			else {
				vv.exeReturn = that._execute.apply(that, 
					[ '.ChromeStndAsOneColStndHolderVcc', 'switchOnOffFooterOffset', [ false ] ]);		
				vv.exeReturn = that._execute.apply(that, 
					[ '.ContentHolderWithPlayerVcc', 'switchOnOffPlayer', [ false ] ]);
			}
		}
	//}

	return ClassConstructor;
})();
