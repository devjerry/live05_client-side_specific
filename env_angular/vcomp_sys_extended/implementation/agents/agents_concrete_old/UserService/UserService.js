/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: UserServiceDef.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

//	INLINE: 
//{
	var aa = {};
		
	//	Equated dependencies.
	//{
		///aa.baseServiceDefMp = require("../aa_baseModelServiceDef/aa_baseModelServiceDefFnc.js");
	//}

	aa.datasCache = {};

	return bgetPublicMethods
//}



//	PRIVATE METHODS:
//{
	function bgetCacheKey(filters, usertype) {
		//	TODO
	}
	
	function TEMP_bgetMockData(type) {
		var datas;
		if ( type === 'recentStationsFromIdsListOfUser' ) {
			datas = [
					{ name: 'Stones Best', id: 'idstones' },
					{ name: 'Beatles Best', id: 'idbeatles' },
					{ name: 'Who Best', id: 'idwho' }
			];
		}
		else if ( type === 'favoriteStationsFromIdsListOfUser' ) {
			datas = [
				{ name: 'Beatles Best', id: 'idbeatles' },
				{ name: 'Zep Best', id: 'idzep' },
				{ name: 'Rush Best', id: 'idrush' }
			];
		};
		
		return datas;
	}
//}


//	PUBLIC METHODS:
//{

	function bgetPublicMethods(injections) {
		var publicMethods = {};
		
		publicMethods.getListings = getListings;
		function getListings(api) {
			var vv = {}
			vv.injections = injections;
			vv.api = api || {}
			vv.filters = api.filters 			|| 'all'; 		/* array */
			vv.userType = api.userType 			|| 'public';	/* string */
			vv.useMockData = api.useMockData 	|| false;		/* boolean true to use mock data (that's provided) */
			vv.mockData = api.mockData  		|| null;		/* provide mock (JSON) data. */

			if ( vv.useMockData ) {
				vv.datas = TEMP_bgetMockData( vv.filters.split()[0] );
			}	

			return vv.datas;
		}
		
		return publicMethods;
	}
	//}

	
})();

