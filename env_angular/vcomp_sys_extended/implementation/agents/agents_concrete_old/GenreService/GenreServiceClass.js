/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: GenreServiceDef.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

//	INLINE: 
//{
	var aa = {};
		
	//	Equated dependencies.
	//{
		///aa.baseServiceDefMp = require("../aa_baseModelServiceDef/aa_baseModelServiceDefFnc.js");
	//}

	aa.datasCache = {};

	return bgetPublicMethods
//}



//	PRIVATE METHODS:
//{
	function bgetCacheKey(filters, usertype) {
		//	TODO
	}
	
	function TEMP_bgetMockData() {
		var datas = [
			{ name: 'Alternative', id: 'idalternative' },
			{ name: 'Blues', id: 'idblues' },
			{ name: 'Classical', id: 'idclassical' },
			{ name: 'Country', id: 'idcountry' },
			{ name: 'Easy Listening', id: 'ideasy' },
			{ name: 'Electronic/Dance', id: 'idelectronic' },
			{ name: 'Folk', id: 'idfolk' },
			{ name: 'Freeform', id: 'idfreeform' },
			{ name: 'Hip-Hop/Rap', id: 'ihip' },
			{ name: 'Inspirational', id: 'idinspirational' },
			{ name: 'Jazz', id: 'idjazz' },
			{ name: 'Latin', id: 'idlatin' },
			{ name: 'Metal', id: 'idmetal' },
			{ name: 'New Age', id: 'idnew' },
			{ name: 'Oldies', id: 'idoldies' },
			{ name: 'Pop', id: 'idpop' },
			{ name: 'R&B/Urban', id: 'idrb' },
			{ name: 'Reggae', id: 'idreggae' },
			{ name: 'Rock', id: 'idrock' },
			{ name: 'Seasonal/Holiday', id: 'iddeasonal' },
			{ name: 'Soundtracks', id: 'idsoundtracks' },
			{ name: 'Talk', id: 'idtalk' }
		];
		
		return datas;
	}
//}


//	PUBLIC METHODS:
//{

	function bgetPublicMethods(injections) {
		var publicMethods = {};
		
		publicMethods.getListings = getListings;
		function getListings(api) {
			var vv = {}
			vv.injections = injections;
			vv.api = api || {}
			vv.filters = api.filters 			|| 'all'; 		/* array */
			vv.userType = api.userType 			|| 'public';	/* string */
			vv.useMockData = api.useMockData 	|| false;		/* boolean true to use mock data (that's provided) */
			vv.mockData = api.mockData  		|| null;		/* provide mock (JSON) data. */

			if ( vv.useMockData ) {
				vv.datas = TEMP_bgetMockData();
			}	

			return vv.datas;
		}
		
		return publicMethods;
	}
	//}

	
})();

