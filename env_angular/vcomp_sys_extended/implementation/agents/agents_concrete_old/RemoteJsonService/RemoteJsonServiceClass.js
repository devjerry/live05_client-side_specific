/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * File: RemoteJsonServiceDef.js
 * *********************************************************************
 * *********************************************************************
 */
module.exports = (function() {

//	INLINE: 
//{
	var aa = {};
		
	aa.Config = {
	};
	
	aa.ResourceClass = NgResource(
		///"https://api.themoviedb.org/3/movie/:mainId?api_key=:apiKey",
		'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:about/:id',
		{
			callback: "JSON_CALLBACK",
			about:'@aboout',
			id:'@id'
		},
		{
			fetch: {
				method: "JSONP",
				isArray: false
			}
		}
	);

	return getPublicMethods
//}


//	====================================================================
//	PRIVATE METHODS:
//{

	function getDataRoutine(urlPattern, urlParams) {
		var vv = {}
		vv.resourceXob = returnResource(urlPattern, urlParams);
		vv.dataPromise = vv.resourceXob.$fetch();
		
	}

	function returnResource(urlPattern, urlParams) {
		vv = {};
		vv.urlPattern = urlPattern || 'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:about/:id'
		vv.ResourceClass = NgResource(
			///"https://api.themoviedb.org/3/movie/:mainId?api_key=:apiKey",
			vv.urlTemplate,
			{
				callback: "JSON_CALLBACK",
				about:'@about',
				id:'@id'
			},
			{
				fetch: {
					method: "JSONP",
					isArray: false
				}
			}
		);
		vv.resourceXob = new vv.ResourceClass({mainId: urlParams.about, apiKey: urlParams.id});
		return vv.resourceXob; 		
	}

//}

//	====================================================================
//	PUBLIC METHODS :
//{

		aa.publicMethods = {};
		
		publicMethods.getData = getDataPub;
		function getDataPub(urlPattern, urlParams) {
			var returnn = getDataRoutine(urlPattern, urlParams) 
			return returnn;
		};
	}
//}

//	====================================================================
//	PUBLIC METHODS "FACTORY":
//{

	function getPublicMethods(injections) {
		return aa.publicMethods;
	}
//}

	
})();

