/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: AppVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	 module.exports

	//	INLINE: 
	//{
		//	Scope for this script.
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(1);
			__webpack_require__(2);
			__webpack_require__(3);
			__webpack_require__(4);
			__webpack_require__(5);
			__webpack_require__(14);
		//}
		
		//	Equated dependencies.
		//{
			aa.configFnc = __webpack_require__(6); 
			aa.CtrlClass = __webpack_require__(7);
			aa.RouterPrvdrClass = __webpack_require__(8); 
			aa.MediatorServiceClass = __webpack_require__(9);
			aa.RemoteDatasServiceClass = __webpack_require__(10);
			aa.StationsServiceClass = __webpack_require__(11);
			aa.GenresServiceClass = __webpack_require__(12);
			aa.UsersServiceClass = __webpack_require__(13);
			
		//}

		//	Create module:
		//{
			aa.thisModName = 'AppVcc';
			aa.thisMod = aa.AppVcc = angular.module(aa.thisModName, [
				'ngResource', 
				'ui.router',
				'BodyContentHolderVcc',
				'ContentHolderWithPlayerVcc',
				'ChromeStndAsOneColStndHolderVcc',
				'Page4HomeVcc',
				'Page4StationDtlVcc',
				'ClickToMediatorAdc',
				'slick'
			]);
		//}
		
		//	Set module's config:
		//{	
			aa.thisMod.config( [
				'$locationProvider',
				'$resourceProvider',
				'$sceDelegateProvider', 
				'$stateProvider', 
				'$urlRouterProvider',
				'AppVccRouterProvider',
				aa.configFnc
			] );
		//}
		
		//	Set modules (app's) services:
		//{	
			aa.thisMod.service('mediatorService', ['$timeout', aa.MediatorServiceClass]);
			aa.thisMod.service('remoteDatasService', ['$resource', aa.RemoteDatasServiceClass]);
			aa.thisMod.service('stationsService', ['$q', '$resource', aa.StationsServiceClass]);
			aa.thisMod.service('genresService', ['$q', '$resource', aa.GenresServiceClass]);
			aa.thisMod.service('usersService', ['$q', '$resource', aa.UsersServiceClass]);
		//}
			
		//	Set module's (only) controller:
		//{	
			aa.ctrlName = 'AppVccVwCtrl';
			createCtrl(aa.thisMod, aa.ctrlName) 
			function createCtrl(module, ctrlName) {
				var vv = {};
				vv.CtrlClass = aa.CtrlClass
				vv.ctrlInjects = ['$scope'];
				vv.CtrlClass.$inject = vv.ctrlInjects;
				module.controller(ctrlName, vv.CtrlClass);
			}	
		//}

		
		//	Set provider of module router:
		//{	
			aa.thisMod.provider( 'AppVccRouter', aa.RouterPrvdrClass, aa );
		//}
		
	//}



/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: BodyContentHolderVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(35);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(45);
			aa.LinkFncClass = __webpack_require__(15);
			aa.CtrlClass = __webpack_require__(16);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'BodyContentHolderVcc';
			aa.thisMod = aa.BodyContentHolderVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctBodyContentHolderVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ContentHolderWithPlayerVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(37);
			__webpack_require__(17);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(46);
			aa.LinkFncClass = __webpack_require__(18);
			aa.CtrlClass = __webpack_require__(19);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'ContentHolderWithPlayerVcc';
			aa.thisMod = aa.ContentHolderWithPlayerVcc = angular.module(aa.thisModsName, [
				'PlayerPanelVcc'
			]);
			
			aa.drctName = 'drctContentHolderWithPlayerVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction(compileNg) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr, compileNg);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ChromeStndAsOneColStndHolderVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(39);
			__webpack_require__(20);
			__webpack_require__(21);
			__webpack_require__(22);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(47);
			aa.LinkFncClass = __webpack_require__(23);
			aa.CtrlClass = __webpack_require__(24);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'ChromeStndAsOneColStndHolderVcc';
			aa.thisMod = aa.ChromeStndAsOneColStndHolderVcc = angular.module(aa.thisModsName, [
				'Header4StndUseVcc',
				'HierarchyNavStripVcc',
				'Footer4StndUseVcc'
			]);
			
			aa.drctName = 'drctChromeStndAsOneColStndHolderVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4HomeVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(41);
			__webpack_require__(25);
			__webpack_require__(26);
			__webpack_require__(27);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(48);
			aa.LinkFncClass = __webpack_require__(28);
			aa.CtrlClass = __webpack_require__(29);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'Page4HomeVcc';
			aa.thisMod = aa.Page4HomeVcc = angular.module(aa.thisModsName, [
				'MastheadPanelVcc',
				'GenresPanelVcc',
				'StationsPanelVcc'
			]);
			
			aa.drctName = 'drctPage4HomeVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', '$state', '$stateParams', '$timeout', 'mediatorService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4StationDtlVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(43);
			__webpack_require__(30);
			__webpack_require__(31);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(49);
			aa.LinkFncClass = __webpack_require__(32);
			aa.CtrlClass = __webpack_require__(33);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'Page4StationDtlVcc';
			aa.thisMod = aa.Page4StationDtlVcc = angular.module(aa.thisModsName, [
				'StationInfoPanelVcc',
				'ChatPanelVcc'
			]);
			
			aa.drctName = 'drctPage4StationDtlVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', '$state', '$stateParams', '$timeout', 'mediatorService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: AppVccConfigFnc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
		function configFnc(LocationProvider, ResourceProvider, SceDelegateProvider, StateProvider, UrlRouterProvider, RouterProvider) {
			var cc = {};
			
			LocationProvider
				.html5Mode(false)
				.hashPrefix('!');

			cc.Router = RouterProvider.getRouter(StateProvider, UrlRouterProvider);
			
			// Don't strip trailing slashes from calculated URLs
			ResourceProvider.defaults.stripTrailingSlashes = false;	
			
			// Execute settting of routes.
			cc.Router.setRoutes();
			
			//	Whitelist/permit access to certain sites.
			SceDelegateProvider.resourceUrlWhitelist([
			   'self',
			   "http://pi-web01.nanocosm.com**"
			]);
		}
		
		return configFnc;
	})();


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: AppVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		};
		
		//	Prototype its methods.
		//{	
			var p = CtrlClass.prototype;
			
			p.setup = function setup() {
				var that = this;
				that.setupDatas();
			};
			
			p.setupDatas = function setupDatas() {
				var that = this;
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none'
						},
						info: {
							name: 'Jerry'
						}
					}
				};
			};
		//}
		
		return CtrlClass;
		
	})();


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: AppVccRouterPrvdrClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
		

		function RouterPrvdrClass() {
			return this;
		};
		//	Prototype its methods.
		//{	
			var p = RouterPrvdrClass.prototype;
			
			p.getRouter = function getRouterFnc(StateProvider, UrlRouterProvider) {	
				//	Inits: 
				//{
					//	Scope for this script.
					var vv = {};
					vv.methods = {};
				//}
				

				//	Define methods.
				//{	
					vv.methods.setRoutes = function setRoutes_fnc() {
						
						// For any unmatched url, redirect to /state1
						UrlRouterProvider.otherwise('/home');
		
						var ContentHolderWithPlayerAbsState = {
							name: 'ContentHolderWithPlayerAbsState',
							abstract : true,
							views: {
								'BodyContentHolderSlot': {
									template: '<drct-content-holder-with-player-vcc></drct-content-holder-with-player-vcc>'
								}
							}
						};
										
						var ChromeStndAsOneColStndHolderAbsState = {
							///onEnter: [function(){ alert('Hello from ChromeStndAsOneColStndHolderAbsState'); }],
							parent: ContentHolderWithPlayerAbsState,
							name: 'ChromeStndAsOneColStndHolderAbsState',
							abstract : true,
							views: {
								'ContentHolderByPlayerSlot@ContentHolderWithPlayerAbsState': {
									template: '<drct-chrome-stnd-as-one-col-stnd-holder-vcc></drct-chrome-stnd-as-one-col-stnd-holder-vcc>'
								}
							}
						};

						var Page4HomeState = {
							///onEnter: [function(){ alert('Hellow from HomePageState'); }],
							parent: ChromeStndAsOneColStndHolderAbsState,
							name: 'Page4HomeState',
							url: '/home',
							views: {
								'ChromeStndAsOneColStndHolderSlot@ChromeStndAsOneColStndHolderAbsState': {
									template: '<drct-page-4-home-vcc></drct-page-4-home-vcc>'
								}
							}
						};
						var Page4StationDtlState = {
							parent: ChromeStndAsOneColStndHolderAbsState,
							name: 'Page4StationDtlState',
							url: '/station{slash1:[/]?}{stationName:[^\/]*}{slash2:[/]?}{stationId:[^\/]*}',
							views: {
								'ChromeStndAsOneColStndHolderSlot@ChromeStndAsOneColStndHolderAbsState': {
									template: '<drct-page-4-station-dtl-vcc></drct-page-4-station-dtl-vcc>'
								}
							}
						};

						StateProvider.state(ContentHolderWithPlayerAbsState);
						StateProvider.state(ChromeStndAsOneColStndHolderAbsState);
						StateProvider.state(Page4HomeState);
						StateProvider.state(Page4StationDtlState);
					};
				//}
				
				return vv.methods;
			};
				
			p.$get = function getFnc() {};  // Not needed for usage, but needed by Angular.
		//}

		return RouterPrvdrClass;
	})();




/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: MediatorServiceClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function ClassConstructor(timeoutNg) {
			var that = this;
			that.timeoutNg = timeoutNg;
			that._setup();
			var breakpoint = 'a';
		}

		var p = ClassConstructor.prototype;


		//	================================================================
		//	PUBLIC METHODS:
		//{	
			p.receiveNotice = receiveNotice;
			function receiveNotice(senderScope, pkg) {
				var that = this;
				var vv = {};
				vv.handleNoticeReturn = that._handleNotice(senderScope,pkg);
				return vv.handleNoticeReturn;
			}
		//}
		
		
		
		//	================================================================
		//	PRIVATE METHODS:
		//{
		
			p._setup = _setup;
			function _setup() {
			}

			p._handleNotice = _handleNotice;
			function _handleNotice(senderScope, pkg) {
				var that = this;
				var vv = {};
				console.log(">>> _handleNotice: ", senderScope, pkg.notice.noticeName, pkg);
				vv.noticeName = pkg.notice.noticeName;	
				if (that["_run__" + vv.noticeName + "__Xnt"] ) {
					vv.noticeTransactionName = "_run__" + vv.noticeName + "__Xnt";
				}
				else if (that["_run__" + vv.noticeName + "__Nnt"] ) {
					vv.noticeTransactionName = "_run__" + vv.noticeName + "__Nnt";
				}
				
				vv.transReturn = that[vv.noticeTransactionName](senderScope, pkg);
				
				return vv.transReturn
			}


			p._execute = _execute;
			function _execute(targetSelector, targetMethodName, methodArgs) {
				var that = this;
				var vv = {};	
				console.log(">>> mediator._execute: ", targetSelector, targetMethodName, methodArgs);
				if ( angular.element( document.querySelector( targetSelector ) ).scope() ) {
					vv.thisc = angular.element( document.querySelector( targetSelector ) ).scope().thisc;
					vv.method = vv.thisc[targetMethodName];
					vv.methodReturn = vv.method.apply(vv.thisc, methodArgs);
				}
				else {
					console.log(">>> COULD NOT EXECUTE.");
				}
				return vv.methodReturn;
			}
		//}
		
		
		//	================================================================
		//	E(X)TERNAL Notice Transactions: (Xnt)  
		//	...These are for notices that originate *outide* of mediator.
		//{
			p._run__Changed_PageState__Xnt = function(senderScope, pkg) {
				console.log(">>> START: _run__Changed_PageState__Xnt");
				var that = this;
				var vv = {}
				
				//{	Needed params: 
					vv.pageStateName = pkg.notice.pageStateName;
				//}
				
				//	Set display on Hierarchy nav strip.
				that._setDisplayOfHierarchyNavStrip__Rtn();
				
				//	Notify what page changed to in order to delegate to
				//	... the transaction per that specific page type.
				pkg.notice.noticeName = "ChangedTo_" + pkg.notice.pageStateName;
				vv.handleNoticeReturn = that.receiveNotice(senderScope, pkg);
				
				//	Manager Footer Spacing
				that._autoManagePlayerAndFooterDisplay__Rtn();

				console.log(">>> END: _run__Changed_PageState__Xnt");
				return vv.rtnReturn;
			}
			
			p._run__Clicked_StationPlaySwitch__Xnt = function(senderScope, pkg) {
				console.log(">>> START: _run__Clicked_StationPlaySwitch__Xnt");
				var that = this;
				var vv = {}
				
				//{	Needed params: 
					vv.stationId = pkg.notice.stationId;
					vv.doPlayFlag = pkg.notice.doPlayFlag || 1;  //  Values are 1 & 0.
				//}
				
				
				//	Set station on player per station for this page.
				vv.rtnReturn = that._setStationOnPlayer__Rtn(vv.stationId, vv.doPlayFlag)
				
				console.log(">>> END: _run__Clicked_StationPlaySwitch__Xnt");
				return vv.rtnReturn;
			}
		//}
		

		//	================================================================
		//	I(N)TERNAL Notice Transactions: (Nnt)  
		//	...These are for notices that originate *inside* of mediator.
		//{
			p._run__ChangedTo_Page4HomeState__Nnt = function(senderScope, pkg) {
				console.log(">>> START: _run__ChangedTo_Page4HomeState__Nnt");
				var that = this;
				var vv = {}
				
				//	LOGIC TBD.
				console.log(">>> END: _run__ChangedTo_Page4HomeState__Nnt");
			}
			
			p._run__ChangedTo_Page4StationDtlState__Nnt = function(senderScope, pkg) {
				console.log(">>> START: _run__ChangedTo_Page4StationDtlState__Nnt");
				var that = this;
				var vv = {}
				
				//{	Needed params: 
					vv.stationId = pkg.notice.stationId;
					vv.stationName = pkg.notice.stationName;
				//}
				
				//	Set station on player per station for this page.
				///vv.rtnReturn = that._setStationOnPlayer__Rtn(vv.stationId)
				
				console.log(">>> END: _run__ChangedTo_Page4StationDtlState__Nnt");
				///return vv.rtnReturn;
			}
		//}
		
		
		
		//	================================================================
		//	ROUTINES: (Rtn)   
		//	...These are basically "partial transactions", and/or 
		//	...simple transactions around executing just one method.
		//{
			
			p._setStationOnPlayer__Rtn = function(stationId, doPlayFlag) {
				var that = this;
				var vv = {}
				console.log(">>> _setStationOnPlayer__Rtn: ", stationId)
				vv.exeReturn = that._execute.apply(that, 
					[ '.ChromeStndAsOneColStndHolderVcc', 'switchOnOffFooterOffset', [ true ] ]);		
				vv.exeReturn = that._execute.apply(that, 
					[ '.ContentHolderWithPlayerVcc', 'switchOnOffPlayer', [ true ] ]);
				vv.exeReturn = that._execute.apply(that, 
					[ '.PlayerPanelVcc', 'setStation', [stationId, doPlayFlag] ]);

				return vv.exeReturn;
			}
			
			p._setDisplayOfHierarchyNavStrip__Rtn = function() {
				var that = this;
				var vv = {}
				vv.exeReturn = that._execute.apply(that, 
					[ '.HierarchyNavStripVcc', 'setDisplay', [] ])

				return vv.exeReturn;
			}
			
			p._autoManagePlayerAndFooterDisplay__Rtn = function() {
				var that = this;
				var vv = {}

				//	Query player panel controller to see if display switch is on or off.
				vv.doesPlayerHaveStation = that._execute.apply(that, 
					[ '.PlayerPanelVcc', 'getStationId', [] ])	
				
				
				//	If player display switch is on, then instruct for gap under footer.
				//ContentHolderWithPlayerVcc
				if ( vv.doesPlayerHaveStation ) {
					vv.exeReturn = that._execute.apply(that, 
						[ '.ChromeStndAsOneColStndHolderVcc', 'switchOnOffFooterOffset', [ true ] ]);		
					vv.exeReturn = that._execute.apply(that, 
						[ '.ContentHolderWithPlayerVcc', 'switchOnOffPlayer', [ true ] ]);
				}
				//	Else no gap under footer (default state)
				else {
					vv.exeReturn = that._execute.apply(that, 
						[ '.ChromeStndAsOneColStndHolderVcc', 'switchOnOffFooterOffset', [ false ] ]);		
					vv.exeReturn = that._execute.apply(that, 
						[ '.ContentHolderWithPlayerVcc', 'switchOnOffPlayer', [ false ] ]);
				}
			}
		//}

		return ClassConstructor;
	})();


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: RemoteDatasServiceClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function ClassConstructor(resourceNg) {
			var that = this;
			that.resourceNg = resourceNg;
		}

		var p = ClassConstructor.prototype;


		//	================================================================
		//	PRIVATE METHODS:
		//{

			p._bgetResourceClass = _bgetResourceClass;
			function _bgetResourceClass(resourceIdfier) {
				var that = this;
				var vv = {};
				vv.resourceIdfier = resourceIdfier || 'live365Default';
				if  (vv.resourceIdfier === 'live365' ||  vv.resourceIdfier === 'live365Default') {

					vv.endpointPattern = 'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:_modelType/:_id';
					
					vv.ResourceClass = that.resourceNg (
						vv.endpointPattern,
						{
							_modelType:'@_modelType',
							_id:'@_id'
						},	
						{ 
							getItemsJSONP: { 
								method: "JSONP", 
								callback: "JSON_CALLBACK",
								isArray: true
								/* 	^ The callback is auto added on to url like this
								 * 	...?callback=JSON_CALLBACK .
								 * 	...Could also do it like this: params{ callback: 'JSON_CALLBACK' } .
								 */
							} 
						} 
					);
				}
				return vv.ResourceClass; 		
			}

		//}

		//	====================================================================
		//	PUBLIC METHODS :
		//{

			p.bgetResourceClass = bgetResourceClass;
			function bgetResourceClass(resourceIdfier) {
				var that = this;
				var ResourceClass = that._bgetResourceClass(resourceIdfier);
				return ResourceClass;
			};
		//}


		return ClassConstructor;
		
	})();

	firstName = {val: "Jerry"};
	lastName = {val: "Simonian"}


/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsServiceClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function ClassConstructor(qNg, resourceNg) {
			var that = this;
			that.qNg = qNg;
			that.resourceNg = resourceNg;
			var breakpoint = 'a';
		}

		var p = ClassConstructor.prototype;

		//	================================================================
		//	PRIVATE METHODS:
		//{
			p._bgetMainResource = _bgetMainResource;
			function _bgetMainResource() {
				var that = this;

				if ( ! that.MainResource ) { 
					that.MainResource = that.resourceNg(
						///	ALT: 'http://pi-node.nanocosm.com:3000/metadata/:modelType/:id?:q1Name:q1Val',
						///'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:modelType/:id?:q1Name:q1Val', 
						'http://pi-api.nanocosm.com:8780/apis/:modelType/:modelSubtype/:id?:q1Name:q1Val', 
						{
							modelType:'stations',
							modelSubtype:'@modelSubtype',
							id:'@id',
							q1Name:'@q1Name',
							q1Val:'@q1Val'
						},
						{
							getListings: { 
								cache: true, 
								method: 'get', 
								isArray: true,
								params: {modelSubtype: 'active'}
							},
							getTopStoriesStations: { 
								cache: true, 
								method: 'get', 
								isArray: true,
								params: {modelSubtype: 'active', q1Name: 'filter=', q1Val: 'topStories'},
							},
							
							getTopStations: { 
								cache: true, 
								method: 'get', 
								isArray: true,
								params: {modelSubtype: 'active', q1Name: 'filter=', q1Val: 'top'},
							},

							getStation: { 
								cache: true, 
								method: 'get', 
								isArray: false
							}
						}
					);
				}

				return that.MainResource;
			}
		//}
		
		//	================================================================
		//	PUBLIC METHODS:
		//{
			
			p.getListings = getListings;
			function getListings() {
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getListings();

				return vv.datas
			}
			
			p.getTopStoriesStations = getTopStoriesStations;
			function getTopStoriesStations() {
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getTopStoriesStations();

				return vv.datas
			}
			
			p.getTopStations = getTopStations;
			function getTopStations() {
				console.log(">>> In getTopStations");
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getTopStations();

				return vv.datas
			}
			
			p.getStation = getStation;
			function getStation(id) {
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getStation({id: id});

				return vv.datas
			}
		//}

		return ClassConstructor;
	})();



/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresServiceClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function ClassConstructor(qNg, resourceNg) {
			var that = this;
			that.qNg = qNg;
			that.resourceNg = resourceNg;
			var breakpoint = 'a';
		}

		var p = ClassConstructor.prototype;

		//	================================================================
		//	PRIVATE METHODS:
		//{
			p._bgetMainResource = _bgetMainResource;
			function _bgetMainResource() {
				var that = this;

				if ( ! that.MainResource ) { 
					that.MainResource = that.resourceNg(
						/// ALT: 'http://pi-node.nanocosm.com:3000/metadata/:modelType/:id',
						'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:modelType/:id', 
						{
							modelType:'genres',
							id:'@id'
						},
						{
							getListings: { 
								cache: true, 
								method: 'get', 
								isArray: true ,
							}
						}
					);
				}

				return that.MainResource;
			}
		//}
		
		//	================================================================
		//	PUBLIC METHODS:
		//{
			
			p.getListings = getListings;
			function getListings() {
				
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getListings();

				return vv.datas
			}
		//}

		return ClassConstructor;
	})();



/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: UsersServiceClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function ClassConstructor(qNg, resourceNg) {
			var that = this;
			that.qNg = qNg;
			that.resourceNg = resourceNg;
			var breakpoint = 'a';
		}

		var p = ClassConstructor.prototype;

		//	================================================================
		//	PRIVATE METHODS:
		//{
			p._bgetMainResource = _bgetMainResource;
			function _bgetMainResource() {
				var that = this;

				if ( ! that.MainResource ) { 
					that.MainResource = that.resourceNg(
						/// ALT: 'http://pi-node.nanocosm.com:3000/metadata/:modelType/:id',
						'http://jco--datas--test.jerryls.com/(chopps-dynm)/data/:modelType/:id/?:q1Name:q1Val', 
						{
							modelType:'users',
							id:'@id',
							q1Name:'@q1Name',
							q1Val:'@q1Val'
						},
						{
							getListings: { 
								cache: true, 
								method: 'get', 
								isArray: true
							},
							getItem: { 
								cache: true, 
								method: 'get', 
								isArray: false
							},
							getFavoriteStations: { 
								cache: true, 
								method: 'get', 
								isArray: true,
								params: {q1Name: 'filter=', q1Val: 'favoriteStations'}
							},
							getRecentStations: { 
								cache: true, 
								method: 'get', 
								isArray: true,
								params: {q1Name: 'filter=', q1Val: 'recentStations'},
							}
						}
					);
				}

				return that.MainResource;
			}
		//}
		
		//	================================================================
		//	PUBLIC METHODS:
		//{
			
			p.getListings = getListings;
			function getListings() {
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getListings();
				return vv.datas
			}
			
			p.getMainItem = getMainItem;
			function getMainItem(id) {
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getItem( { id: id } );
				return vv.datas
			}
			
			
			p.getFavoriteStations = getFavoriteStations;
			function getFavoriteStations(id) {
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getFavoriteStations( { id: id } );
				return vv.datas
			}
			

			p.getRecentStations = getRecentStations;
			function getRecentStations(id) {
				var that = this;
				var vv = {}
				vv.MainResource = that._bgetMainResource();
				vv.datas = vv.MainResource.getRecentStations( { id: id } );
				return vv.datas
			}
			

		//}

		return ClassConstructor;
	})();




/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ClickToMediatorAdc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 console.log("boooooo");
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
		//}
		
		//	Equated dependencies.
		//{
			aa.LinkFncClass = __webpack_require__(34);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'ClickToMediatorAdc';
			aa.thisMod = aa.ClickToMediatorAdc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctClickToMediatorAdc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['mediatorService', drctFunction]);	
		}
		
		function drctFunction(mediatorService) {
			console.log("boooooo2");
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr, mediatorService);
			}
			//	Define injection into the Ctrl class.
			///vv.controller = ['$scope', 'mediatorService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'A',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: BodyContentHolderVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: BodyContentHolderVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'BodyContentHolderVcc',
						time: Date.now()
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: PlayerPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(71);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(73);
			aa.LinkFncClass = __webpack_require__(50);
			aa.CtrlClass = __webpack_require__(51);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'PlayerPanelVcc';
			aa.thisMod = aa.PlayerPanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctPlayerPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction(compileNg) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNg, elm, attr) { 
				new vv.LinkFncClass(scopeNg, elm, attr, compileNg);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ContentHolderWithPlayerVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr, compileNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.compileNg = compileNg;
			console.log(">>> ContentHolderWithPlayerVccLinkFncClass. ");
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that._setScopeWatchers();
			
		};
		
		
		p._setClickHandler = function _setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///console.log(">>>");
					}
				}
			)
		};
		
		
		p._setScopeWatchers = function _setScopeWatchers() {
			var that = this;
			console.log(">>> ContentHolderWithPlayerVccLinkFncClass watch 1. ");
			//--------------------------------------------------------------
			//	Watch datas.view.info.iframeSrc
			var handleScopeWatch__isOkayToShowPlayer__async = function(newVal, oldVal) {
				console.log(">>> handleScopeWatch__isOkayToShowPlayer__async", newVal, oldVal);
				that._handleScopeWatch__isOkayToShowPlayer.call(that, newVal, oldVal)
			}
			
			that.scopeNg.$watch('datas.view.mech.isOkayToShowPlayer', handleScopeWatch__isOkayToShowPlayer__async, true);
			console.log(">>> ContentHolderWithPlayerVccLinkFncClass watch 2. ");
		}
		
		
		p._handleScopeWatch__isOkayToShowPlayer = function _handleScopeWatch__isOkayToShowPlayer(newVal, oldVal) {
			console.log(">>> _handleScopeWatch__isOkayToShowPlayer 1. ");
			var that = this;
			that._showPlayerPanel();
		};
		
		
		p._showPlayerPanel = function _showPlayerPanel() {
			var that = this;
			console.log(">>> _showPlayerPanel: ", that.elm[0]);
			that.compileMeElmo = $(that.elm[0].querySelector('.playerPanelWrap'));
			that.compileNg(that.compileMeElmo.contents())(that.scopeNg);
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ContentHolderWithPlayerVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg ) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that._setup();
		}
			
		var p = CtrlClass.prototype;
		
		//	================================================================
		//	PUBLIC METHODS:
		//{
			p.switchOnOffPlayer = function switchOnOffFooterOffset( doSwitchOn ) {
				var that = this;
				if ( doSwitchOn ) {
					///that.scopeNg.datas.view.mech.isOkayToShowPlayer = false;
					that.scopeNg.datas.view.mech.isOkayToShowPlayer = true;
				}
				else {
					that.scopeNg.datas.view.mech.isOkayToShowPlayer = false;
					///that.scopeNg.datas.view.mech.isOkayToShowPlayer = true;
				}
				that.scopeNg.$apply();
				console.log(">>> that.scopeNg.datas.view.mech.isOkayToShowPlayer: ", that.scopeNg.datas.view.mech.isOkayToShowPlayer, that);
				
			}
		//}
		
		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function setup() {
				var that = this;
				that._setupDatas();
			};
			
			p._setupDatas = function _setupDatas() {
				var that = this;
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none',
							isOkayToShow: true,
							isOkayToShowPlayer: false
						},
						info: {
							myname: 'ContentHolderWithPlayerVcc',
							time: Date.now()
						}
					}
				};
			};
		//}

		return CtrlClass;
		
	})();





/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Header4StndUseVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(74);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(76);
			aa.LinkFncClass = __webpack_require__(52);
			aa.CtrlClass = __webpack_require__(53);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'Header4StndUseVcc';
			aa.thisMod = aa.Header4StndUseVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctHeader4StndUseVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: HierarchyNavStripVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(77);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(79);
			aa.LinkFncClass = __webpack_require__(54);
			aa.CtrlClass = __webpack_require__(55);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'HierarchyNavStripVcc';
			aa.thisMod = aa.HierarchyNavStripVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctHierarchyNavStripVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', '$state', '$stateParams', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Footer4StndUseVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(80);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(82);
			aa.LinkFncClass = __webpack_require__(56);
			aa.CtrlClass = __webpack_require__(57);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'Footer4StndUseVcc';
			aa.thisMod = aa.Footer4StndUseVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctFooter4StndUseVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ChromeStndAsOneColStndHolderVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ChromeStndAsOneColStndHolderVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that._setup()
		}
			
		var p = CtrlClass.prototype;
		
		//	================================================================
		//	PUBLIC METHODS:
		//{
			p.switchOnOffFooterOffset = function switchOnOffFooterOffset( doSwitchOn ) {
				var that = this;
				if ( doSwitchOn ) {
					that.scopeNg.datas.view.mech.string_footerOffset_yesOrNo = 'footerOffset_yes';
				}
				else {
					that.scopeNg.datas.view.mech.string_footerOffset_yesOrNo = 'footerOffset_no';
				}
				
			}
		//}

		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function _setup() {
				var that = this;
				that._setupDatas();
			};
			
			p._setupDatas = function _setupDatas() {
				var that = this;
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none',
							isOkayToShow: true, 
							string_footerOffset_yesOrNo: 'footerOffset_yes'
						},
						info: {
							myname: 'ChromeStndAsOneColStndHolderVcc',
							time: Date.now()
						}
					}
				};
			};

		return CtrlClass;
		
		//}
		
	})();





/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: MastheadPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(83);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(85);
			aa.LinkFncClass = __webpack_require__(58);
			aa.CtrlClass = __webpack_require__(59);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'MastheadPanelVcc';
			aa.thisMod = aa.MastheadPanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctMastheadPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 //
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(86);
			__webpack_require__(60);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(88);
			aa.LinkFncClass = __webpack_require__(61);
			aa.CtrlClass = __webpack_require__(62);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'GenresPanelVcc';
			aa.thisMod = aa.GenresPanelVcc = angular.module(aa.thisModsName, [
				'GenresListingsVcc'
			]);
			
			aa.drctName = 'drctGenresPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}s
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(89);
			__webpack_require__(63);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(91);
			aa.LinkFncClass = __webpack_require__(64);
			aa.CtrlClass = __webpack_require__(65);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'StationsPanelVcc';
			aa.thisMod = aa.StationsPanelVcc = angular.module(aa.thisModsName, [
				'StationsListingsVcc'
			]);
			
			aa.drctName = 'drctStationsPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@',
					atrbListingsLayoutType: '@',
					atrbPanelDisplayedTitle: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4HomeVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4HomeVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, stateNg, stateParamsNg, timeoutNg, mediatorService ) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that.stateNg = stateNg;
			that.stateParamsNg = stateParamsNg;
			that.timeoutNg = timeoutNg;
			that.mediatorService = mediatorService;
			that._setup();
			that._notifyChangedPageState(that.stateParamsNg);
			

		}
		
		var p = CtrlClass.prototype;
		
		//	================================================================
		//	PUBLIC METHODS:
		//{}	
		
		
		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function setup() {
				var that = this;
				that._setupDatas();
			};
			
			p._setupDatas = function _setupDatas() {
				var that = this;
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none'
						},
						info: {
							myname: 'Page4HomeVcc'
						}
					}
				};
			};
		
			p._notifyChangedPageState = function _notifyChangedPageState() {
				var that = this;
				var pkg = {
					notice: {
						noticeName: 'Changed_PageState',
						pageStateName: that.stateNg.current.name,
						stationId: that.stateParamsNg.stationId,
						stationName: that.stateParamsNg.stationName
					}
				}
				var asyncNotify = function() {
					that.mediatorService.receiveNotice(that.scopeNg, pkg);
				}
				that.timeoutNg(asyncNotify);
			}
		//}

		return CtrlClass;
		
	})();





/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationInfoPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(92);
			__webpack_require__(66);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(94);
			aa.LinkFncClass = __webpack_require__(67);
			aa.CtrlClass = __webpack_require__(68);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'StationInfoPanelVcc';
			aa.thisMod = aa.StationInfoPanelVcc = angular.module(aa.thisModsName, [
				'SocialingPanelVcc',
			]);
			
			aa.drctName = 'drctStationInfoPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', 'stationsService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbStationId: '@',
					atrbStationName: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ChatPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(95);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(97);
			aa.LinkFncClass = __webpack_require__(69);
			aa.CtrlClass = __webpack_require__(70);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'ChatPanelVcc';
			aa.thisMod = aa.ChatPanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctChatPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction(compileNg) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNg, elm, attr) { 
				new vv.LinkFncClass(scopeNg, elm, attr, compileNg);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', '$state', '$stateParams', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbChannelId: '@',
					atrbUserDisplayName: '@'	
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	


/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4StationDtlVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4StationDtlVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, stateNg, stateParamsNg, timeoutNg, mediatorService ) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that.stateNg = stateNg;
			that.stateParamsNg = stateParamsNg;
			that.timeoutNg = timeoutNg;
			that.mediatorService = mediatorService;
			that._setup();
			that._notifyChangedPageState();
		}
			
		var p = CtrlClass.prototype;
		
		//	================================================================
		//	PUBLIC METHODS:
		//{}	
		
		
		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function setup() {
				var that = this;
				that._setupDatas();
			};
			
			p._setupDatas = function setupDatas() {
				var that = this;
				console.log( ">>> Page4StationDtlVccCtrlClass - stationId - stationName: ",that.stateParamsNg.stationId, that.stateParamsNg.stationName, that.mediatorService);
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none',
							_atrbStationId: that.stateParamsNg.stationId,
							_atrbStationName: that.stateParamsNg.stationName,
							_atrbUserDisplayName: "Spud Bud "+Date.now()
						},
						info: {
							myname: 'Page4StationDtlVcc'
						}
					}
				};
			};
			
			p._notifyChangedPageState = function _notifyChangedPageState() {
				var that = this;
				var pkg = {
					notice: {
						noticeName: 'Changed_PageState',
						pageStateName: that.stateNg.current.name,
						stationId: that.stateParamsNg.stationId,
						stationName: that.stateParamsNg.stationName
					}
				}
				var asyncNotify = function() {
					that.mediatorService.receiveNotice(that.scopeNg, pkg);
				}
				that.timeoutNg(asyncNotify);
			}


		//}

		return CtrlClass;
		
	})();




/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ClickToMediatorAdcLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr, mediatorService) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.mediatorService = mediatorService;
			that._setup();
			console.log("boooooo3: ");
		};
			
		var p = LinkFncClass.prototype;
		
		p._setup = function _setup() {
			var that = this;
			that._setClickHandler();
		};
		
		
		p._setClickHandler = function _setClickHandler() {
			var that = this;
			
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(evnt) {
						var vv = {};
						vv.pkgForMediator = angular.fromJson(that.attr.drctClickToMediatorAdc);
						vv.pkgForMediator.evnt = evnt;
						//	Send package to mediator
						//{	
							console.log(">>> handleClick_fnc - vv.pkgForMediator: ", vv.pkgForMediator, that);
							that.mediatorService.receiveNotice("", vv.pkgForMediator);
						//}
					}
				}
			)
		};

		return LinkFncClass;
		
	})();



/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(36);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/BodyContentHolderVcc/BodyContentHolderVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/BodyContentHolderVcc/BodyContentHolderVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: BodyContentHolderVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \n .whoAmI {\n\tdisplay: none;\n }\n \n .vcBox {\n\t/** border: dotted 1px #b77; **/\n }\n \n* {\n\t/** Layout **/\n\tmargin: 0;\n\tpadding: 0;\n }\n \n  h1, h2, h3, h4, h5 {\n\t/** Layout **/\n\t/** Style **/\n\tfont-weight: normal;\n }\n \n body {\n\t/** Layout **/\n\tmin-height:100%;\n\t\n\t/** Style **/\n}\n \ndrct-body-content-holder-vcc {\n\t/** Layout **/\n\tdisplay: block;\n\n\n\t\n\t/** Style **/\n\tfont-family: Verdana, Geneva, sans-serif;\n\tfont-size: 12px;\n}\n\n", ""]);

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(38);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/ContentHolderWithPlayerVcc/ContentHolderWithPlayerVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/ContentHolderWithPlayerVcc/ContentHolderWithPlayerVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: ContentHolderWithPlayerVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \n\n \ndrct-content-holder-with-player-vcc {\n\t/** Layout **/\n\tdisplay: block;\n}\n\n.PlayerPanelVcc {\n\t/** Layout **/\n\tmargin-bottom__: 104px;\n}\n\n.drct-content-holder-with-player-vcc ._holder {\n}\n\ndrct-content-holder-with-player-vcc  drct-player-panel-vcc{\n\t/** Layout **/\n\tposition: fixed;\n\tbottom: 0;\n\tleft: 0;\n\tz-index: 10000;\n}\n\n\n\n", ""]);

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(40);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/ChromeStndAsOneColStndHolderVcc/ChromeStndAsOneColStndHolderVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/ChromeStndAsOneColStndHolderVcc/ChromeStndAsOneColStndHolderVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: ChromeStndAsOneColStndHolderVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-chrome-stnd-as-one-col-stnd-holder-vcc {\n\t/** Layout **/\n\tdisplay: block;\n\tposition: relative;\n\tmin-height: 100vh;\n\t\n\t/** Style **/\n\tbackground-color: #ddc;\n}\n\n\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc  drct-header-4-stnd-use-vcc {\n\t/** Layout **/\n\tmargin-bottom: 10px;\n}\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc  drct-hierarchy-nav-strip-vcc {\n\t/** Layout **/\n\tmargin-left: 20px;\n\tmargin-right: 20px;\n\tmargin-bottom: 30px;\n\t\n\t/** Style **/\n\tborder__: solid 1px red;\n}\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc  ._holder {\n\t/** Layout **/\n\tmargin-left: 40px;\n\tmargin-right: 40px;\n}\n\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc .footerOffset_yes  .bottomOffsetter {\n\theight: calc(104px + 105px);\n}\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc  .footerOffset_no  .bottomOffsetter {\n\theight: calc(104px + 60px);\n}\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc  drct-footer-4-stnd-use-vcc {\n\t/** Layout **/\n\tposition: absolute;\n\tbottom: 0px;\n\theight: 55px;\n\twidth: calc( 100% - (0px + 0px) );\n\t\n\t/** Style **/\n\tborder__: solid 1px orange;\n}\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc  .footerOffset_yes  drct-footer-4-stnd-use-vcc {\n\t/** Layout **/\n\tbottom: 104px;\n}\n\ndrct-chrome-stnd-as-one-col-stnd-holder-vcc  .footerOffset_no  drct-footer-4-stnd-use-vcc {\n\t/** Layout **/\n\tbottom: 0px;\n}\n\n", ""]);

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(42);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Page4HomeVcc/Page4HomeVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Page4HomeVcc/Page4HomeVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: Page4HomeVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n\ndrct-page-4-home-vcc {\n\tdisplay: block;\n}\n\ndrct-page-4-home-vcc  drct-masthead-panel-vcc {\n\tmargin-bottom: 30px;\n}\n\ndrct-page-4-home-vcc  drct-stations-panel-vcc {\n\tmargin-bottom: 30px;\n}\n", ""]);

/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(44);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Page4StationDtlVcc/Page4StationDtlVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Page4StationDtlVcc/Page4StationDtlVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: Page4StationDtlVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-page-4-station-dtl-vcc {\n\tdisplay: block;\n}\n\n\ndrct-page-4-station-dtl-vcc  drct-chat-panel-vcc {\n\tpadding-top: 20px;\n\tborder-top: solid 1px #bbb;\n}\n", ""]);

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: BodyContentHolderVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<div class=\"whoAmI\">This is the {{datas.view.info.myname}} Tmplt.  Time: {{datas.view.info.time}}.</div>\n\n<div class=\"_holder vcBox\"\n\tdata-ui-view='BodyContentHolderSlot' \n>\n\tThis is the inside of {{datas.view.info.myname}} \n</div>\n\n";

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: ContentHolderWithPlayerVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n\n<div class=\"ContentHolderWithPlayerVcc vcBox\">\n\t\n\t<div class=\"displaySwitch\"\n\t\tng-if=\"datas.view.mech.isOkayToShow\"\n\t>\n\t\t<div class=\"whoAmI\">This is the {{datas.view.info.myname}} Tmplt.  Time: {{datas.view.info.time}} .</div>\n\n\n\n\t\t<div class=\"_holder\"\n\t\t\tdata-ui-view='ContentHolderByPlayerSlot' \n\t\t>\n\t\t\tThis is the inside of {{datas.view.info.myname}} \n\t\t</div>\n\n\t\t<div class=\"playerPanelWrap\">\n\t\t\t<drct-player-panel-vcc \n\t\t\t\tng-if=\"datas.view.mech.isOkayToShowPlayer\"\n\t\t\t></drct-player-panel-vcc>\n\t\t</div>\n\t</div>\n</div>\n\n\n\n\n";

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: ChromeStndAsOneColStndHolderVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt.  Time: {{datas.view.info.time}} . -->\n<div class=\"ChromeStndAsOneColStndHolderVcc vcBox {{datas.view.mech.string_footerOffset_yesOrNo}}\"> \n\t<div class=\"displaySwitch\"\n\t\tng-if=\"datas.view.mech.isOkayToShow\"\n\t\t\n\t>\n\n\t\t<drct-header-4-stnd-use-vcc></drct-header-4-stnd-use-vcc>\n\t\t<drct-hierarchy-nav-strip-vcc></drct-hierarchy-nav-strip-vcc>\n\n\t\t<div data-ui-view='ChromeStndAsOneColStndHolderSlot' class=\"_holder\">\n\t\t\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\t\t</div>\n\n\t\t<div class=\"bottomOffsetter\"></div>\n\n\t\t<drct-footer-4-stnd-use-vcc></drct-footer-4-stnd-use-vcc>\n\t</div>\n</div>\n\n";

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: Page4HomeVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n\n<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\n<drct-masthead-panel-vcc></drct-masthead-panel-vcc>\n\n<drct-stations-panel-vcc \n\tclass=\"gridLayout allStations\"\n\tatrb-panel-displayed-title=\"All Stations\" \n\tatrb-listings-type=\"topStations\" \n\tatrb-listings-layout-type=\"grid\" \n></drct-stations-panel-vcc>\n\n\n<drct-stations-panel-vcc \n\tclass=\"standardCarousel recentlyPlayedStations\"\n\tatrb-panel-displayed-title=\"Recently Played Stations\"\n\tatrb-listings-type=\"recentStationsFromIdsListOfUser\"\n\tatrb-listings-layout-type=\"standardCarousel\" \n></drct-stations-panel-vcc>\n\n\n";

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: Page4StationDtlVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n\n<div class=\"Page4StationDtlVcc vcBox\">\n\t\n\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\t\t\n\t<drct-station-info-panel-vcc \n\t\tatrb-station-id=\"{{datas.view.mech._atrbStationId}}\" \n\t\tatrb-station-name=\"{{datas.view.mech._atrbStationName}}\"\n\t></drct-station-info-panel-vcc>\n\t\n\t<drct-chat-panel-vcc \n\t\tatrb-channel-id=\"{{datas.view.mech._atrbStationId}}\"\n\t\tatrb-user-display-name=\"{{datas.view.mech._atrbUserDisplayName}}\" \n\t></drct-chat-panel-vcc>\n\n</div>\n\n\n\n";

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: PlayerPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr, compileNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.compileNg = compileNg;
			that._setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p._setup = function _setup() {
			var that = this;
			that._setClickHandler();
			that._setScopeWatchers();
		};
		
		
		p._setClickHandler = function _setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///console.log(">>>");
					}
				}
			)
		};
		
		
		p._setScopeWatchers = function _setScopeWatchers() {
			var that = this;
			//--------------------------------------------------------------
			//	Watch datas.view.info.iframeSrc
			var handleScopeWatch__iframeSrc__async = function(newVal, oldVal) {
				that._handleScopeWatch__iframeSrc.call(that, newVal, oldVal)
			}
			that.scopeNg.$watch('datas.view.info.iframeSrc', handleScopeWatch__iframeSrc__async, true);
		}
		
		
		p._handleScopeWatch__iframeSrc = function _handleScopeWatch__iframeSrc(newVal, oldVal) {
			var that = this;
			that._addViewportIframe();
		};
		
		
		p._addViewportIframe = function _addViewportIframe() {
			var that = this;
			that.iframeElmo = $(that.elm[0].querySelector('.viewportIframe'));
			if  ( that.iframeElmo ) {
				that.iframeElmo.remove();
			}
			
			var displaySwitchElmo = $(that.elm[0].querySelector('.displaySwitch'));
			
			displaySwitchElmo.append('<iframe class="viewportIframe" ng-src="{{datas.view.info.iframeSrc}}" scrolling="no"></iframe>');
			
			that.compileNg(displaySwitchElmo.contents())(that.scopeNg);
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: PlayerPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg ) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that._setup();
		}
			
		var p = CtrlClass.prototype;
		
		//	================================================================
		//	PUBLIC METHODS:
		//{
			p.setStation = function setStation(stationId, doPlayFlag) {
				var that = this;
				that.scopeNg.datas.view.info.stationId = stationId;
				that.scopeNg.datas.view.info.doPlayFlag = doPlayFlag;  	// Valid options are 0 or 1.
				that.scopeNg.datas.view.info.iframeSrc = that._bgetIframeSrc();
				that.scopeNg.$apply();
			}
			
			p.getStationId = function getStationId() {
				var that = this;
				return that.scopeNg.datas.view.info.stationId;
			}
		//}
		
		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function setup() {
				var that = this;
				that._setupDatas();
			};
			
			p._setupDatas = function setupDatas() {
				var that = this;
				that.mech = {};
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none',
							isOkayToShow: true

						},
						info: {
							myname: 'PlayerPanelVcc',
							stationId: "",
							doPlayFlag: 0,		// Valid options are 0 or 1.
							iframeSrc: ""
						}
					}
				};

				///that.scopeNg.datas.view.info.iframeSrc  = that._bgetIframeSrc();
			};
			
			p._bgetIframeSrc = function getIframeSrc() {
				var that = this;
				var vv = {};
				
				///return "http://pi-web01.nanocosm.com/?station_id=7448"
				console.log(">>> that.scopeNg.datas.view.info.stationId: ", that.scopeNg.datas.view.info.stationId);
				
				if ( that.scopeNg.datas.view.info.stationId ) {
					vv.newSrcUrl = "http://pi-web01.nanocosm.com/?station_id="+that.scopeNg.datas.view.info.stationId;
				}
				else {
					vv.newSrcUrl = that.mech.currentStationId || "http://pi-web01.nanocosm.com/?station_id=7448";
				}
				
				if (that.scopeNg.datas.view.info.doPlayFlag === 1) {
					vv.newSrcUrl = vv.newSrcUrl + "&play=" + 1;
				}
				
				
				console.log(">>> In that.scopeNg.methods.getIframeSrc - vv.newSrcUrl: ", vv.newSrcUrl) 
				
				that.mech.currentStationId = vv.newSrcUrl;
				return vv.newSrcUrl;

			};
		//}


		return CtrlClass;
		
	})();


/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Header4StndUseVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Header4StndUseVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'Header4StndUseVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: HierarchyNavStripVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: HierarchyNavStripVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, stateNg, stateParamsNg) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that.stateNg = stateNg;
			that.stateParamsNg = stateParamsNg;
			that._setup()
			that._setViewDataPerState();
		}
			
		var p = CtrlClass.prototype;
		
		//	================================================================
		//	PUBLIC METHODS:
			p.setDisplay = function setDisplay() {
				var that = this;
				that._setViewDataPerState();
			}
		//{}	
		
		
		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function _setup() {
				var that = this;
				that._setupDatas();
			};
			
			p._setupDatas = function _setupDatas() {
				var that = this;
				that.scopeNg.datas = {
					control: {},
					external: {},
					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none'
						},
						info: {
							myname: 'HierarchyNavStripVcc'
						}
					}
				};
			};
			
			p._setViewDataPerState = function _setViewDataPerState() {
				var that = this;
				var vv = {};
				vv.currentStateName = that.stateNg.current.name;
				
				vv.statesViewData = that._bgetStatesViewData();
				vv.stateViewData = vv.statesViewData[vv.currentStateName];
				if (vv.stateViewData) {
					that.scopeNg.datas.view.mech.isOkayToShow = vv.stateViewData.isOkayToShow;
					that.scopeNg.datas.view.info.navThisPageName = vv.stateViewData.navThisPageName;
				}

			};
			
			p._bgetStatesViewData = function _bgetStatesViewData() {
				var that = this;
				var vv = {};
				
				vv.statesViewData = {
					Page4HomeState: {
						navThisPageName: "home",
						isOkayToShow: false 
					},
					Page4StationDtlState: {
						navThisPageName: "station info",
						isOkayToShow: true
					}
					
				};
				
				return vv.statesViewData;
			};
			
		//}

		return CtrlClass;
		
	})();





/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Footer4StndUseVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Footer4StndUseVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'Footer4StndUseVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: MastheadPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 59 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: MastheadPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'MastheadPanelVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresListingsVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 //
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(106);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(108);
			aa.LinkFncClass = __webpack_require__(100);
			aa.CtrlClass = __webpack_require__(101);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'GenresListingsVcc';
			aa.thisMod = aa.GenresListingsVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctGenresListingsVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
		
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', 'genresService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						atrbListingsType: that.scopeNg.atrbListingsType
					},
					info: {
						myname: 'GenresPanelVcc',
						listingsTitle: that.scopeNg.atrbListingsType
					}
				}
			};
		};
		
		
		return CtrlClass;
		
	})();






/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsListingsVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 //
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(109);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(111);
			aa.LinkFncClass = __webpack_require__(102);
			aa.CtrlClass = __webpack_require__(103);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'StationsListingsVcc';
			aa.thisMod = aa.StationsListingsVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctStationsListingsVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
		
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', '$timeout', 'stationsService', 'usersService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@',
					atrbLayoutType: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						atrbListingsType: that.scopeNg.atrbListingsType
					},
					info: {
						myname: 'StationsPanelVcc',
						listingsTitle: that.scopeNg.atrbListingsType
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: SocialingPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(112);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(114);
			aa.LinkFncClass = __webpack_require__(104);
			aa.CtrlClass = __webpack_require__(105);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'SocialingPanelVcc';
			aa.thisMod = aa.SocialingPanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctSocialingPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationInfoPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 68 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationInfoPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, stationsService) {
			var that = this;
			that.scopeNg = scopeNg;
			that.stationsService = stationsService;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {};
			that.scopeNg.datas.control = {};
			that.scopeNg.datas.external = {}
			that.scopeNg.datas.external.station = {};
			
			that.scopeNg.datas.external.station = that.getStation(that.scopeNg.atrbStationId);
			console.log(">>> that.scopeNg.atrbStationId ", that.scopeNg.atrbStationId);
			console.log(">>> StationInfoPanelVccCtrlClass - station (a): ", that.scopeNg.datas.external.station);

			that.scopeNg.datas.control = {};
			that.scopeNg.datas.view = {
				mech: {
					pleaseWaitDisplayStyle: 'none'
				},
				info: {
					myname: 'StationInfoPanelVcc',
					station: that.scopeNg.datas.external.station
				}
			};
			///console.log(">>> StationInfoPanelVccCtrlClass - station (b): ", that.scopeNg.datas.external.station);
		};
		
		p.getStation = function getStation(id) {	
			var that = this;
			var vv = {};
			vv.data = that.stationsService.getStation(id);

			return vv.data;
		};

		return CtrlClass;
		
	})();





/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ChatPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr, compileNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.compileNg = compileNg;
			that._setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p._setup = function _setup() {
			var that = this;
			that._setClickHandler();
			that._setScopeWatchers();
		};
		
		p._setClickHandler = function _setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///console.log(">>>");
					}
				}
			)
		};
		
		
		p._setScopeWatchers = function _setScopeWatchers() {
			var that = this;
			//--------------------------------------------------------------
			//	Watch datas.view.info.iframeSrc
			var handleScopeWatch__iframeSrc__async = function(newVal, oldVal) {
				that._handleScopeWatch__iframeSrc.call(that, newVal, oldVal)
			}
			that.scopeNg.$watch('datas.view.info.iframeSrc', handleScopeWatch__iframeSrc__async, true);
		}
		
		
		p._handleScopeWatch__iframeSrc = function _handleScopeWatch__iframeSrc(newVal, oldVal) {
			var that = this;
			that._addViewportIframe();
		};
		
		p._addViewportIframe = function _addViewportIframe() {
			var that = this;
			that.iframeElmo = $(that.elm[0].querySelector('.viewportIframe'));
			if  ( that.iframeElmo ) {
				that.iframeElmo.remove();
			}
			
			var displaySwitchElmo = $(that.elm[0].querySelector('.displaySwitch'));
			
			$(displaySwitchElmo).append('<iframe class="viewportIframe chatIframe" ng-src="{{datas.view.info.iframeSrc}}" scrolling="no"></iframe>');
			
			that.compileNg($(displaySwitchElmo).contents())(that.scopeNg);
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: ChatPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, stateNg, stateParamsNg ) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that.stateNg = stateNg;
			that.stateParamsNg = stateParamsNg;
			that._setup();
		}
			
		var p = CtrlClass.prototype;
		
		//	================================================================
		//	PUBLIC METHODS:
		//{
		//}
		
		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function setup() {
				var that = this;
				that._setupDatas();
			};
			
			p._setupDatas = function setupDatas() {
				var that = this;
				that.mech = {};
				that.scopeNg.datas = {
					control: {},
					external: {},

					view: {
						mech: {
							pleaseWaitDisplayStyle: 'none',
							isOkayToShow: true

						},
						info: {
							myname: 'ChatPanelVcc',
							channelId: '',
							username: '',
							iframeSrc: ""
						}
					}
				};

				that.scopeNg.datas.view.info.iframeSrc  = that._bgetIframeSrc(that.scopeNg.atrbChannelId, that.scopeNg.atrbUserDisplayName);
			};
			
			p._bgetIframeSrc = function bgetIframeSrc(channelId, userDisplayName) {
				var that = this;
				var vv = {};

				vv.newSrcUrl = "Chatty/Chatty.html?channelId="+channelId+"&userDisplayName="+userDisplayName;

				return vv.newSrcUrl;

			};
		//}


		return CtrlClass;
		
	})();


/***/ },
/* 71 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(72);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/PlayerPanelVcc/PlayerPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/PlayerPanelVcc/PlayerPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: PlayerPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n\n\ndrct-player-panel-vcc {\n\t/** Layout **/\n\tdisplay: block;\n\twidth: calc( 100% );\n\tmin-width: 425px;\n\t\n\t/** Style **/\n\tbackground-color: #000;\n}\n\ndrct-player-panel-vcc .requestedInfo {\n\t/** Layout **/\n\tmargin: 5px;\n\t/** Style **/\n\tcolor: #ddf;\n    text-decoration: none;\n    font-family: Verdana, Geneva, sans-serif;\n    font-size: 9px;\n}\n\n\ndrct-player-panel-vcc .viewportIframe {\n\t/** Layout **/\n\twidth: calc(100% - (5px + 5px) );\n\tmin-width: 425px;\n\theight: 70px;\n\tpadding: 0;\n\tmargin: 5px;\n\toverflow: hidden;\n\t\n\t/** Style **/\n\tborder: none;\n}\n\n\n", ""]);

/***/ },
/* 73 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: PlayerPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<div class=\"PlayerPanelVcc vcBox\">\n\t\n\t<div class=\"displaySwitch\"\n\t\tng-if=\"datas.view.mech.isOkayToShow\"\n\t>\n\t\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\n\t\t<div class=\"requestedInfo\">\n\t\t\tStationId requested: {{datas.view.info.stationId}}.\n\t\t\tPlayerUrl: {{datas.view.info.iframeSrc}}.\n\t\t</div>\n\t\t\n\t</div>\n</div>\n\n\n";

/***/ },
/* 74 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(75);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Header4StndUseVcc/Header4StndUseVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Header4StndUseVcc/Header4StndUseVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: Header4StndUseVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-header-4-stnd-use-vcc {\n\tdisplay: block;\n}\n\n.Header4StndUseVcc {\n\t/** Layout **/\n\twidth: 100%;\n\theight: 64px;\n\t\n\t/** Style **/\n\tbackground-color: #000;\n}\n\n\n.Header4StndUseVcc .logoImg {\n\t/** Layout **/\n\t\t/*- for vertical centering -*/\n\t\tposition: relative;  \n\t\ttop: 50%; transform: \n\t\ttranslateY(-50%);\n\tmargin-left: 10px;\n\t\t\n\t/** Style **/\n\tborder: solid 1px #755;\n\t\n\n}\n", ""]);

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: Header4StndUseVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<div class=\"whoAmI\">This is the {{datas.view.info.myname}} Tmplt. </div>\n<div  class=\"Header4StndUseVcc vcBox\">\n\t<img class=\"logoImg\" src=\"http://dummyimage.com/5:2x50/000000/ffffff&text=Live365\" alt=\"Logo image for header\">\n</div>\n\n";

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(78);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/HierarchyNavStripVcc/HierarchyNavStripVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/HierarchyNavStripVcc/HierarchyNavStripVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 78 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: HierarchyNavStripVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-hierarchy-nav-strip-vcc {\n\tdisplay: block;\n}\n\n.HierarchyNavStripVcc {\n\t/** Layout **/\n\t/** Style **/\n\ttext-transform: uppercase;\n}\n\n.HierarchyNavStripVcc .navLink {\n\t/** Layout **/\n\t/** Style **/\n    text-decoration: none;\n    color: #33e;\n}\n\n\n.HierarchyNavStripVcc .navLink:after {\n\t/** Layout **/\n\t/** Style **/\n    content: \" | \";\n}\n\n\n/*\n<div  class=\"HierarchyNavStripVcc\">\n\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\t<a class=\"navLink\" href=\"#!/home\" title\"HOME\">HOME</a><span class=navSeperator\"> | </span><span class=navThisPage\">STATION INFO</span>\n</div>\n*/\n", ""]);

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: HierarchyNavStripVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n\n<div class=\"HierarchyNavStripVcc vcBox\"> \n\t<div class=\"displaySwitch\"\n\t\tng-if=\"datas.view.mech.isOkayToShow\"\n\t>\n\n\t\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\t\n\t\t<a class=\"navLink\" href=\"#!/home\" title\"HOME\">home</a><span class=navThisPage\">{{datas.view.info.navThisPageName}}</span>\n\t\n\t</div>\n</div>\n\n";

/***/ },
/* 80 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(81);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Footer4StndUseVcc/Footer4StndUseVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/Footer4StndUseVcc/Footer4StndUseVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: Footer4StndUseVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-footer-4-stnd-use-vcc {\n\t/** Layout **/\n\tdisplay: block;\n\tmin-height: 55px;\n\n\t/** Style **/\n\tcolor: #aaa;\n\tbackground-color: #333;\n}\n\n\ndrct-footer-4-stnd-use-vcc .usAndCopyright{\n\t/** Layout **/\n\ttext-align: center;\n\t\t/*- for vertical centering -*/\n\t\tposition: relative;  \n\t\ttop: 50%; transform: \n\t\ttranslateY(-50%);\n\n\t/** Style **/\n}\n\ndrct-footer-4-stnd-use-vcc .usAndCopyright .us {\n\tmargin-bottom: 5px;\n}\n\ndrct-footer-4-stnd-use-vcc .usAndCopyright .copyright {\n}\n", ""]);

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: Footer4StndUseVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<div class=\"usAndCopyright\">\n\t<div class=\"us\">\n\t\tLive365.com is an officially licensed ASCAP, BMI, SESAC, SoundExchange, & SOCAN site.\n\t</div>\n\t<div class=\"copyright\">\n\t\tCopyright © Live365, Inc. 1999-2015\n\t</div>\n</div>\n\n\n";

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(84);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/MastheadPanelVcc/MastheadPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/MastheadPanelVcc/MastheadPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: MastheadPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-masthead-panel-vcc {\n\t/** Layout **/\n\tdisplay: block;\n\twidth: calc(100%);\n    padding:0;\n    text-align: left;\n    position: relative;\n    /** Style **/\n}\n\n\ndrct-masthead-panel-vcc .mastheadImg {\n\t/** Layout **/\n\tdisplay: block;\n    width: 100%;\n    height: calc(100% * (333 / 977) );\n    margin-bottom: 0;\n\tpadding-bottom: 0;\n\tborder__: solid 1px red;\n}\n", ""]);

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: MastheadPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<div class=\"whoAmI\">This is the {{datas.view.info.myname}} Tmplt. </div>\n\n<!--\n<img class=\"mastheadImg\" src=\"http://dummyimage.com/16:9x200/775555/bbbbbb&text=Masthead\" alt=\"Image for Masthead\">\n-->\n\n<img class=\"mastheadImg\" src=\"http://projects.jerryls.com/LiveO5/Assets/homepage_image.jpg\" alt=\"Image for Masthead\">\n\n";

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(87);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/GenresPanelVcc/GenresPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/GenresPanelVcc/GenresPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: GenresPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-genres-panel-vcc {\n\tdisplay: block;\n\tpadding: 0px;\n\tcolor: #33a;\n\tborder: dotted 0px #33a;\n}\n\n.GenresPanelVcc { \n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33d;\n\tborder: dotted 1px #33d;\n}\n.GenresPanelVcc .GenresListingsVcc > ul {\n    padding:0;\n    margin:0;\n    text-align:center;\n}\n.GenresPanelVcc .GenresListingsVcc > ul > li {\n\tmargin: 2px;\n\tpadding: 2px;\n    display:inline-block;\n    vertical-align:top;\n    position:relative;\n    background:grey;\n    color: white;\n}\n.GenresPanelVcc .GenresListingsVcc > ul > li > a {\n    color: white;\n    text-decoration: none;\n}\n", ""]);

/***/ },
/* 88 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: GenresPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"GenresPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<!-- <div>* {{datas.view.info.listingsTitle}}</div> -->\n\n\t<drct-genres-listings-vcc atrb-listings-type=\"{{datas.view.mech.atrbListingsType}}\"></drct-genres-listings-vcc>\n\n</div>\n\n";

/***/ },
/* 89 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(90);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/StationsPanelVcc/StationsPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/StationsPanelVcc/StationsPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: StationsPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-stations-panel-vcc {\n\tdisplay: block;\n}\n\n\n\n/***********************************************************************\n * for Common *\n **********************************************************************/\n\ndrct-stations-panel-vcc .panelDisplayedTitle {\n\t/** Layout **/\n\tmargin-left: calc( 10px );\n\t/** Style **/\n    font-family: Verdana, Geneva, sans-serif;\n    font-size: 16px;\n}\n\n\ndrct-stations-panel-vcc  drct-stations-listings-vcc .listingsCase {\n\t/** Layout **/\n\twidth: calc(100%);\n    padding:0;\n    margin-left__: 20px;\n    margin-right__: 20px;\n    text-align: left;\n    position: relative;\n    /** Style **/\n    border__: solid 1px blue;\n    list-style-type: none;\n}\n\ndrct-stations-panel-vcc  drct-stations-listings-vcc .listingItem {\n\t/** Layout **/\n\tdisplay:inline-block;\n\tpadding: 15px;\n    vertical-align: top;\n    position:relative;\n\t/** Style **/\n\tborder__: solid 1px red;\n\tbackground-color__: #aaf;\n}\n\n\n drct-stations-panel-vcc  drct-stations-listings-vcc .listingLink {\n\t/** Layout **/\n\tdisplay: block;\n\ttext-align: left;\n\tvertical-align: top;\n\t/** Style **/\n    text-decoration: none;\n    border: solid 2px #fff;\n}\n\ndrct-stations-panel-vcc  drct-stations-listings-vcc .listingImg {\n\t/** Layout **/\n\tdisplay: block;\n    width: 100%;\n    height: calc(100% * 9/16);\n    margin-bottom: 0;\n\tpadding-bottom: 0;\n\tborder__: solid 1px red;\n}\n\ndrct-stations-panel-vcc  drct-stations-listings-vcc .listingTitle {\n\t/** Layout **/\n\tpadding: 10px;\n\tpadding-bottom: 15px;\n    overflow: hidden;\n    white-space:nowrap;\n    text-overflow: ellipsis;\n\n\t\n\t/** Style **/\n    color: #555;\n    text-decoration: none;\n    font-family: Verdana, Geneva, sans-serif;\n    font-size: 13px;\n    font-weight: normal;\n    background-color: #ccc;\n    border__: solid 1px green;\n \n}\n\n/***********************************************************************\n * for Grid Layout *\n **********************************************************************/\n\ndrct-stations-panel-vcc.gridLayout  drct-stations-listings-vcc .listingItem {\n\t/** Layout **/\n\twidth: calc( (100% / 1) - (15px + 15px) );\n\tmargin__: 0;\n\twidth__: calc(11 * (100% * 1/5)/12  );\n\tmin-width: 200px;\n\tmargin__: 10px;\n\tmargin__: calc(1 * (100% * 1/5)/12  );\t\n \n}\n\n\n    /*==========  Mobile First Method  ==========*/\n\n    /* Custom, iPhone Retina */ \n    @media only screen and (min-width : 320px) {\n\t\tdrct-stations-panel-vcc.gridLayout  drct-stations-listings-vcc .listingItem {\n\t\t\twidth: calc( (100% / 1) - (15px + 15px) );\n        }\n    }\n\n    /* Extra Small Devices, Phones */ \n    @media only screen and (min-width : 480px) {\n\t\tdrct-stations-panel-vcc.gridLayout  drct-stations-listings-vcc .listingItem {\n\t\t\twidth: calc( (100% / 1) - (15px + 15px) );\n        }\n    }\n\n    /* Small Devices, Tablets */\n    @media only screen and (min-width : 768px) {\n\t\tdrct-stations-panel-vcc.gridLayout  drct-stations-listings-vcc .listingItem {\n\t\t\twidth: calc( (100% / 2) - (15px + 15px) );\n        }\n    }\n\n    /* Medium Devices, Desktops */\n    @media only screen and (min-width : 992px) {\n\t\tdrct-stations-panel-vcc.gridLayout  drct-stations-listings-vcc .listingItem {\n\t\t\twidth: calc( (100% / 3) - (15px + 15px) );\n        }\n    }\n\n    /* Large Devices, Wide Screens */\n    @media only screen and (min-width : 1200px) {\n\t\tdrct-stations-panel-vcc.gridLayout  drct-stations-listings-vcc .listingItem {\n\t\t\twidth: calc( (100% / 4) - (15px + 15px) );\n        }\n    }\n\n\n\n/***********************************************************************\n * for Standard Carousel Layout *\n **********************************************************************/\n\ndrct-stations-panel-vcc.standardCarousel  drct-stations-listings-vcc .listingsCarousel {\n\t/** Layout **/\n\twidth: 100%;\n    margin-bottom: 0;\n\tborder__: solid 1px red;\n}\n\ndrct-stations-panel-vcc.standardCarousel  drct-stations-listings-vcc .listingItem {\n\t/** Layout **/\n\twidth: calc(100% * 1/4);\t\n\t/** Style **/\n}\n\ndrct-stations-panel-vcc.standardCarousel  drct-stations-listings-vcc .slick-prev,\ndrct-stations-panel-vcc.standardCarousel  drct-stations-listings-vcc .slick-next {\n\t/** Style **/\n}\n\ndrct-stations-panel-vcc.standardCarousel  drct-stations-listings-vcc .slick-prev {\n\tleft: 20px;\n}\ndrct-stations-panel-vcc.standardCarousel  drct-stations-listings-vcc .slick-next {\n\tright: 20px;\n}\n\n\n", ""]);

/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: StationsPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<div class=\"StationsPanelVcc vcBox\">\n\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\n\t<h2 class=\"panelDisplayedTitle\">{{atrbPanelDisplayedTitle}}</h2>\n\n\t<drct-stations-listings-vcc \n\t\tatrb-listings-type=\"{{datas.view.mech.atrbListingsType}}\" \n\t\tatrb-layout-type=\"{{atrbListingsLayoutType}}\"\n\t></drct-stations-listings-vcc>\n</div>\n\n\n";

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(93);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/StationInfoPanelVcc/StationInfoPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/StationInfoPanelVcc/StationInfoPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: StationInfoPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-station-info-panel-vcc {\n\tdisplay: block;\n}\n\n\ndrct-station-info-panel-vcc .stationMasthead {\n\t/** Layout **/\n\twidth: 100%;\n\toverflow: hidden;\n\tmargin-bottom: 30px;\n\tborder__: solid 1px green\n}\n\ndrct-station-info-panel-vcc .stationLogoBox {\n\t/** Layout **/\n\tfloat: left;\n}\n\ndrct-station-info-panel-vcc .stationSummary{\n\t/** Layout **/\n\tfloat: left;\n\tdisplay: block;\n\twidth: calc( 100% - (276px + 20px + 20px) );\n\tmargin-left: 20px;\n\tmargin-top: 80px;\n\toverflow: hidden;\n\t\t\n\t/** Style **/\n\tfont-size: 16px;\n\tborder__: solid 1px red;\n}\n\ndrct-station-info-panel-vcc .stationTitle {\n\t/** Layout **/\n\tdisplay: block;\n\tmargin-bottom: 10px;\n\t\t\n\t/** Style **/\n\tfont-size: 16px;\n\tborder__: solid 1px red;\n}\n\ndrct-station-info-panel-vcc .playButton {\n\t/** Layout **/\n\tdisplay: block;\n\t\t\n\t/** Style **/\n\tfont-size: 16px;\n}\n\n\ndrct-station-info-panel-vcc .stationDetailDesc {\n\t/** Layout **/\n\tdisplay: block\n}\n\n\n/*\n<div  class=\"StationInfoPanelVcc\">\n\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\t<div \"stationLogo\">\n\t\t<img src=\"{{datas.view.info.station.stationLogoUrl}}\" alt=\"{{datas.view.info.station.title}} station logo\" >\n\t</div>\n\t<div>\n\t\t<div class=\"stationTitle\">\n\t\t\t{{datas.view.info.station.title}}\n\t\t</div>\n\t</div>\n\t<div class=\"stationDetailDesc\">\n\t\t{{datas.view.info.station.detailDescription}}\n\t</div>\n</div>\n* \n* \n\t** Layout **\n\tmargin: 0;\n\tpadding: 0;\n\ttop: 0;\n\tbottom: 0;\n\tleft: 0;\n\tright: 0;\n\t** Style **\n\tfont-weight: normal;\n\t\n*/\n", ""]);

/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: StationInfoPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n\n\n<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n<div class=\"stationMasthead\">\n\t<div class=\"stationLogoBox\">\n\t\t<img src=\"{{datas.view.info.station.stationLogoUrl}}\" alt=\"{{datas.view.info.station.title}} station logo\" >\n\t</div>\n\t<div class=\"stationSummary\">\n\t\t<h1 class=\"stationTitle\">\n\t\t\t{{datas.view.info.station.title}} {{datas.view.info.station.id}}\n\t\t</h1>\n\t\t<button class=\"playButton\"\n\t\t\tdrct_click_to_mediator_adc = '{\n\t\t\t\t\"notice\": {\n\t\t\t\t\t\"noticeName\": \"Clicked_StationPlaySwitch\",\n\t\t\t\t\t\"vc\": \"StationInfoPanelVcc\",\n\t\t\t\t\t\"context\": \"\",\n\t\t\t\t\t\"stationId\": {{datas.view.info.station.id}}\n\t\t\t\t}\n\t\t\t}'\n\t\t>\n\t\t\t&#9654;\n\t\t</button>\n\t</div>\n</div>\n\n<div class=\"stationDetailDesc\">\n\t{{datas.view.info.station.detailDescription}}\n</div>\n\n\n";

/***/ },
/* 95 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(96);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/ChatPanelVcc/ChatPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/ChatPanelVcc/ChatPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 96 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: ChatPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n\n\ndrct-chat-panel-vcc {\n\t/** Layout **/\n\tdisplay: block;\n\twidth: calc( 100% );\n\tmin-width: 425px;\n\t\n\t/** Style **/\n\tbackground-color: #ddc;\n}\n\ndrct-chat-panel-vcc .requestedInfo {\n\t/** Layout **/\n\tdisplay: none;\n\tmargin: 5px;\n\t/** Style **/\n\tcolor: #ddc;\n    text-decoration: none;\n    font-family: Verdana, Geneva, sans-serif;\n    font-size: 9px;\n}\n\n\ndrct-chat-panel-vcc .viewportIframe {\n\t/** Layout **/\n\twidth: calc(100% - (5px + 5px) );\n\tmin-width: 425px;\n\theight: 100px;\n\tpadding: 0;\n\tmargin: 5px;\n\toverflow: hidden;\n\t\n\t/** Style **/\n\tborder: none;\n}\n\n\n", ""]);

/***/ },
/* 97 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: ChatPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<div class=\"ChatPanelVcc vcBox\">\n\t\n\t<div class=\"displaySwitch\"\n\t\tng-if=\"datas.view.mech.isOkayToShow\"\n\t>\n\t\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\n\t\t<div class=\"requestedInfo\">\n\t\t\tChannelId requested: {{datas.view.info.channelId}}.\n\t\t\tChatUrl: {{datas.view.info.iframeSrc}}.\n\t\t</div>\n\t</div>\n</div>\n\n\n\n\n";

/***/ },
/* 98 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isIE9 = memoize(function() {
			return /msie 9\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0;

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isIE9();

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function createStyleElement() {
		var styleElement = document.createElement("style");
		var head = getHeadElement();
		styleElement.type = "text/css";
		head.appendChild(styleElement);
		return styleElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement());
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else {
			styleElement = createStyleElement();
			update = applyToTag.bind(null, styleElement);
			remove = function () {
				styleElement.parentNode.removeChild(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	function replaceText(source, id, replacement) {
		var boundaries = ["/** >>" + id + " **/", "/** " + id + "<< **/"];
		var start = source.lastIndexOf(boundaries[0]);
		var wrappedReplacement = replacement
			? (boundaries[0] + replacement + boundaries[1])
			: "";
		if (source.lastIndexOf(boundaries[0]) >= 0) {
			var end = source.lastIndexOf(boundaries[1]) + boundaries[1].length;
			return source.slice(0, start) + wrappedReplacement + source.slice(end);
		} else {
			return source + wrappedReplacement;
		}
	}

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(styleElement.styleSheet.cssText, index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(sourceMap && typeof btoa === "function") {
			try {
				css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(JSON.stringify(sourceMap)) + " */";
				css = "@import url(\"data:text/css;base64," + btoa(css) + "\")";
			} catch(e) {}
		}

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}


/***/ },
/* 99 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function() {
		var list = [];
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
		return list;
	}

/***/ },
/* 100 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresListingsVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 101 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresListingsVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, genresService) {
			var that = this;
			that.scopeNg = scopeNg;
			that.genresService = genresService;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {};
			that.scopeNg.datas.control = {};
			that.scopeNg.datas.external = {
				genres: that.getGenres()
			};
			that.scopeNg.datas.view = {
				mech: {
					pleaseWaitDisplayStyle: 'none',
					atrbListingsType: that.scopeNg.atrbListingsType
				},
				info: {
					myname: 'GenresListingsVcc',
					genres: that.scopeNg.datas.external.genres,
					listingsTitle: that.scopeNg.atrbListingsType
				}
			};
			console.log("GenresListings - genres : ", that.scopeNg.datas.view.info.genres);
		};

		p.getGenres = function getGenres() {	
			var that = this;
			var vv = {};
			vv.filters = "all";
			vv.userListingsType = "public";
			vv.useMockData = true;

			vv.datas =  that.genresService.getListings(that.scopeNg);


			
			return vv.datas;
		};
		
		
		return CtrlClass;
		
	})();






/***/ },
/* 102 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsListingsVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 103 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsListingsVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, timeoutNg, stationsService, usersService) {
			scopeNg.thisc = this;
			var that = this;
			that.scopeNg = scopeNg;
			that.timeoutNg = timeoutNg;
			that.stationsService = stationsService;
			that.usersService = usersService;
			that._setup()
		}
			
		var p = CtrlClass.prototype;
		
		
		//	================================================================
		//	PUBLIC METHODS:
		//{}	
		
		
		//	================================================================
		//	PRIVATE METHODS:
		//{	
			p._setup = function _setup() {
				var that = this;
				that._setupDatas();
				that.testUser01Id = 777;
				that.scopeNg.getStations = that.getStations;
			};
			
			p._setPublicMethods = function _setPublicMethods() {
				var that = this;
				that.scopeNg.getStations = that.getStations;
			}
			
			p._setupDatas = function _setupDatas() {
				var that = this;
				that.scopeNg.datas = {};
				that.scopeNg.datas.control = {};
				that.scopeNg.datas.external = {}
				
				that.scopeNg.datas.external.stations = "";
				
			
				
				that.scopeNg.datas.external.stations = that._getStations(that.scopeNg.atrbListingsType)
				
				that.scopeNg.datas.view = {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						atrbListingsType: that.scopeNg.atrbListingsType,
						doHaveListings: false
					},
					info: {
						myname: 'StationsListingsVcc',
						stations: that.scopeNg.datas.external.stations,
						listingsTitle: that.scopeNg.atrbListingsType
					}
				};
				console.log("StationsListings - stations : ", that.scopeNg.datas.view.info.stations);
			
			
				//  This was added because carousel shouldn't be painted until listing
				//	...resource promise resolves, else carousel won't know how to lay out.
				that.scopeNg.$watch('datas.external.stations', function(newVal, oldVal){	
					if ( that.scopeNg.datas.external.stations  &&  that.scopeNg.datas.external.stations[0] ) {
						that.scopeNg.datas.view.mech.isOkayToShow = true; 
					}
				}, true);

			};

			p._getStations = function _getStations(listingsType) {	
				var that = this;
				var vv = {};
				vv.filters = listingsType;
				vv.userListingsType = "public";
				vv.useMockData = true;
				vv.mockUserId = 777;
				
				if ( vv.filters === 'topStations' ) {
					vv.datas = that.stationsService.getTopStations();
				}
				else if  ( vv.filters === 'favoriteStationsFromIdsListOfUser' ) {
					
					vv.datas = that.usersService.getFavoriteStations(vv.mockUserId);
				}
				else if  ( vv.filters === 'recentStationsFromIdsListOfUser' ) {
					vv.datas = that.usersService.getRecentStations(vv.mockUserId);
				}

				return vv.datas;
			};
		//}
		
		return CtrlClass;
		
	})();



/***/ },
/* 104 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: SocialingPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 105 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: SocialingPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'SocialingPanelVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 106 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(107);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/GenresListingsVcc/GenresListingsVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/GenresListingsVcc/GenresListingsVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 107 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: GenresListingsVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-genres-listings-vcc {\n\tdisplay: block;\n\tpadding: 0px;\n\tcolor: #33a;\n\tborder: dotted 0px #33a;\n}\n\n\n.GenresListingsVcc { \n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33d;\n\tborder: dotted 1px #33d;\n}\n", ""]);

/***/ },
/* 108 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: GenresListingsVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"GenresListingsVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<!-- <div>* {{datas.view.info.listingsTitle}}</div> -->  \n\t<ul>\n\t\t<li ng-repeat=\"Listing_ in datas.view.info.genres\"   ng-class=\"{ 'focsed' : Listing_.id == vv.FocsedItem.id}\">\n\t\t\t<a href=\"#/genre/{{Listing_.name}}/{{Listing_.id}}\"  data-vw-model-aspect={{Listing_}} data-listing-id={{Listing_.id}}  >\n\t\t\t\t{{Listing_.name}}\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n\n";

/***/ },
/* 109 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(110);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/StationsListingsVcc/StationsListingsVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/StationsListingsVcc/StationsListingsVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 110 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: StationsListingsVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-stations-listings-vcc {\n\tdisplay: block;\n}\n\n", ""]);

/***/ },
/* 111 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: StationsListingsVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div class=\"StationsListingsVcc vcBox\"  \n\tng-if=\"datas.view.mech.isOkayToShow\"\n>\n\t<div class=\"whoAmI\">This is the inside of {{datas.view.info.myname}}.</div>\n\t<!--- <div>* {{datas.view.info.listingsTitle}}</div> -->  \n\t\n\t<ul class=\"listingsCase\" \n\t\tng-if=\"atrbLayoutType == 'basic'  ||  atrbLayoutType == 'grid' || !atrbLayoutType \"\n\t>\n\t\t\n\t\t<li class=\"listingItem\" \n\t\t\tng-repeat=\"Listing_ in datas.view.info.stations\"   \n\t\t\tng-class=\"{ 'focsed' : Listing_.id == vv.FocsedItem.id}\"\n\t\t\tid=\"listing_{{Listing_.id}}\"\n\t\t>\n\n\t\t\t\t<a class=\"listingLink\" \n\t\t\t\t\thref=\"#!/station/{{Listing_.name}}/{{Listing_.id}}\" \n\t\t\t\t\ttitle=\"{{Listing_.name}}\" \n\t\t\t\t>\n\t\t\t\t\t<img class=\"listingImg\" \n\t\t\t\t\t\tsrc=\"{{Listing_.stationLogoUrl}}\"\n\t\t\t\t\t\talt=\"Image for {{Listing_.name}}\"\n\t\t\t\t\t>\n\t\t\t\t\t<h3 class=\"listingTitle\">{{Listing_.name}}</h3>\n\t\t\t\t</a>\n\n\t\t</li>\n\t</ul>\n\t\n\t<div class=\"listingsCase\">\n\t\t<slick \n\t\t\tclass=\"listingsCarousel\" \n\t\t\tng-if=\"atrbLayoutType == 'standardCarousel'\"  \n\t\t\tinfinite='true' \n\t\t\tslides-to-show='4' \n\t\t\tslides-to-scroll='4' \n\t\t\tlazy-load='ondemand' \n\t\t\tdata=\"datas.view.info.stations\"\n\t\t>\n\t\t\t<div class=\"listingItem\" \n\t\t\t\tng-repeat=\"Listing_ in datas.view.info.stations\"   \n\t\t\t\tng-class=\"{ 'focsed' : Listing_.id == vv.FocsedItem.id}\"\n\t\t\t>\n\n\t\t\t\t<a class=\"listingLink\" \n\t\t\t\t\thref=\"#!/station/{{Listing_.name}}/{{Listing_.id}}\" \n\t\t\t\t\ttitle=\"{{Listing_.name}}_{{$index}}\" \n\t\t\t\t>\n\t\t\t\t\t<img class=\"listingImg\" \n\t\t\t\t\t\tdata-lazy=\"http://dummyimage.com/150x16:9/555555/bbbbbb&text={{Listing_.name}}'s img\" \n\t\t\t\t\t\talt=\"Image for {{Listing_.name}}\"\n\t\t\t\t\t>\n\t\t\t\t\t<h3 class=\"listingTitle\">{{Listing_.name}}_{{$index}}</h3>\n\t\t\t\t</a>\n\n\t\t\t</div>\n\t\t</slick>\n\t</div>\n</div>\n\n";

/***/ },
/* 112 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(113);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(98)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/SocialingPanelVcc/SocialingPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete/SocialingPanelVcc/SocialingPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 113 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(99)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: SocialingPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-socialing-panel-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33a;\n\tborder: dotted 1px #33a;\n}\n\n.SocialingPanelVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: solid 1px #33f;\n}\n", ""]);

/***/ },
/* 114 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: SocialingPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\nThis is the {{datas.view.info.myname}} Tmplt.\n<div  class=\"SocialingPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n</div>\n\n";

/***/ }
/******/ ])