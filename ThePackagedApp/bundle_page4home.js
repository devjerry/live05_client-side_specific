"use strict";

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4HomeVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(6);
			__webpack_require__(1);
			__webpack_require__(2);
			__webpack_require__(3);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(8);
			aa.LinkFncClass = __webpack_require__(4);
			aa.CtrlClass = __webpack_require__(5);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'Page4HomeVcc';
			aa.thisMod = aa.Page4HomeVcc = angular.module(aa.thisModsName, [
				'MastheadPanelVcc',
				'GenresPanelVcc',
				'StationsPanelVcc'
			]);
			
			aa.drctName = 'drctPage4HomeVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: MastheadPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(17);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(19);
			aa.LinkFncClass = __webpack_require__(9);
			aa.CtrlClass = __webpack_require__(10);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'MastheadPanelVcc';
			aa.thisMod = aa.MastheadPanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctMastheadPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 //
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(20);
			__webpack_require__(11);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(22);
			aa.LinkFncClass = __webpack_require__(12);
			aa.CtrlClass = __webpack_require__(13);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'GenresPanelVcc';
			aa.thisMod = aa.GenresPanelVcc = angular.module(aa.thisModsName, [
				'GenresListingsVcc'
			]);
			
			aa.drctName = 'drctGenresPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}s
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(23);
			__webpack_require__(14);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(25);
			aa.LinkFncClass = __webpack_require__(15);
			aa.CtrlClass = __webpack_require__(16);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'StationsPanelVcc';
			aa.thisMod = aa.StationsPanelVcc = angular.module(aa.thisModsName, [
				'StationsListingsVcc'
			]);
			
			aa.drctName = 'drctStationsPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4HomeVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4HomeVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'Page4HomeVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(7);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/Page4HomeVcc/Page4HomeVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/Page4HomeVcc/Page4HomeVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(27)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: Page4HomeVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n\ndrct-page-4-home-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #733;\n\tborder: dotted 1px #733;\n}\n\n.Page4HomeVcc {\n\tpadding: 0px;\n\tcolor: #a00;\n\tborder: solid 0px #a00;\n}\n", ""]);

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: Page4HomeVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"Page4HomeVcc\">\n\tThis is the inside of {{datas.view.info.myname}} \n\t<drct-masthead-panel-vcc></drct-masthead-panel-vcc>\n\t\n\t<drct-genres-panel-vcc atrb-listings-type=\"ALL_GENRES\"></drct-genres-panel-vcc>\n\n\t<drct-stations-panel-vcc atrb-listings-type=\"recentStationsFromIdsListOfUser\"></drct-stations-panel-vcc>\n\n\t<drct-stations-panel-vcc atrb-listings-type=\"favoriteStationsFromIdsListOfUser\"></drct-stations-panel-vcc>\n\n\t<drct-stations-panel-vcc atrb-listings-type=\"topStoriesStations\"></drct-stations-panel-vcc>\n\t\n\t<div>\n\t\t<a href=\"#!/station\">Go to Station Detail Page</a>\n\t</div>\n</div>\n\n";

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: MastheadPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: MastheadPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'MastheadPanelVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresListingsVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 //
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(32);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(34);
			aa.LinkFncClass = __webpack_require__(28);
			aa.CtrlClass = __webpack_require__(29);
			aa.GenreServiceDef = __webpack_require__(38);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'GenresListingsVcc';
			aa.thisMod = aa.GenresListingsVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctGenresListingsVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
		
		
		//	Create Genre Service for this module.
		//	...Note since the service definiton is a singleton it will share (as desired)
		//	...certain properties with creations in other modules.:
		//{
			///aa.tryInjectFnc = function() { alert("Hello from tryInjectFnc") };
			aa.thisMod.factory('genreService', ['$window', aa.GenreServiceDef])
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', 'genreService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						atrbListingsType: that.scopeNg.atrbListingsType
					},
					info: {
						myname: 'GenresPanelVcc',
						listingsTitle: that.scopeNg.atrbListingsType
					}
				}
			};
		};
		
		
		return CtrlClass;
		
	})();






/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsListingsVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 //
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(35);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(37);
			aa.LinkFncClass = __webpack_require__(30);
			aa.CtrlClass = __webpack_require__(31);
			aa.StationServiceDef = __webpack_require__(39);
			aa.UserServiceDef = __webpack_require__(40);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'StationsListingsVcc';
			aa.thisMod = aa.StationsListingsVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctStationsListingsVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
		
		
		//	Create services for this module.
		//	...Note since the service definiton is a singleton it will share (as desired)
		//	...certain properties with creations in other modules.:
		//{
			aa.thisMod.factory('stationService', ['$window', aa.StationServiceDef]);
			aa.thisMod.factory('userService', ['$window', aa.UserServiceDef]);
		//}
		
		//	Create User Service for this module.
		//	...Note since the service definiton is a singleton it will share (as desired)
		//	...certain properties with creations in other modules.:
		//{
			aa.thisMod.factory('userService', ['$window', aa.UserServiceDef])
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', 'stationService',  'userService', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {
					atrbListingsType: '@'
				},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none',
						atrbListingsType: that.scopeNg.atrbListingsType
					},
					info: {
						myname: 'StationsPanelVcc',
						listingsTitle: that.scopeNg.atrbListingsType
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(18);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/MastheadPanelVcc/MastheadPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/MastheadPanelVcc/MastheadPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(27)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: MastheadPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-masthead-panel-vcc {\n\tdisplay: block;\n\tpadding: 0px;\n\tcolor: #33a;\n\tborder: dotted 0px #33a;\n}\n\n.MastheadPanelVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: dotted 1px #33f;\n}\n", ""]);

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: MastheadPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"MastheadPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n</div>\n\n";

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(21);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/GenresPanelVcc/GenresPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/GenresPanelVcc/GenresPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(27)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: GenresPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-genres-panel-vcc {\n\tdisplay: block;\n\tpadding: 0px;\n\tcolor: #33a;\n\tborder: dotted 0px #33a;\n}\n\n.GenresPanelVcc { \n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33d;\n\tborder: dotted 1px #33d;\n}\n.GenresPanelVcc .GenresListingsVcc > ul {\n    padding:0;\n    margin:0;\n    text-align:center;\n}\n.GenresPanelVcc .GenresListingsVcc > ul > li {\n\tmargin: 2px;\n\tpadding: 2px;\n    display:inline-block;\n    vertical-align:top;\n    position:relative;\n    background:grey;\n    color: white;\n}\n.GenresPanelVcc .GenresListingsVcc > ul > li > a {\n    color: white;\n    text-decoration: none;\n}\n", ""]);

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: GenresPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"GenresPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<!-- <div>* {{datas.view.info.listingsTitle}}</div> -->\n\t<drct-genres-listings-vcc atrb-listings-type=\"{{datas.view.mech.atrbListingsType}}\"></drct-genres-listings-vcc>\n\n</div>\n\n";

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(24);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/StationsPanelVcc/StationsPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/StationsPanelVcc/StationsPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(27)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: StationsPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-stations-panel-vcc {\n\tdisplay: block;\n\tpadding: 0px;\n\tcolor: #33a;\n\tborder: dotted 0px #33a;\n}\n\n.StationsPanelVcc { \n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33d;\n\tborder: dotted 1px #33d;\n}\n.StationsPanelVcc .StationsListingsVcc > ul {\n    padding:0;\n    margin:0;\n    text-align:center;\n}\n.StationsPanelVcc .StationsListingsVcc > ul > li {\n\tmargin: 2px;\n\tpadding: 2px;\n    display:inline-block;\n    vertical-align:top;\n    position:relative;\n    background:grey;\n    color: white;\n}\n.StationsPanelVcc .StationsListingsVcc > ul > li > a {\n    color: white;\n    text-decoration: none;\n}\n", ""]);

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: StationsPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"StationsPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<div>* {{datas.view.info.listingsTitle}}</div>\n\t<drct-stations-listings-vcc atrb-listings-type=\"{{datas.view.mech.atrbListingsType}}\"></drct-stations-listings-vcc>\n\n</div>\n\n";

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isIE9 = memoize(function() {
			return /msie 9\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0;

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isIE9();

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function createStyleElement() {
		var styleElement = document.createElement("style");
		var head = getHeadElement();
		styleElement.type = "text/css";
		head.appendChild(styleElement);
		return styleElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement());
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else {
			styleElement = createStyleElement();
			update = applyToTag.bind(null, styleElement);
			remove = function () {
				styleElement.parentNode.removeChild(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	function replaceText(source, id, replacement) {
		var boundaries = ["/** >>" + id + " **/", "/** " + id + "<< **/"];
		var start = source.lastIndexOf(boundaries[0]);
		var wrappedReplacement = replacement
			? (boundaries[0] + replacement + boundaries[1])
			: "";
		if (source.lastIndexOf(boundaries[0]) >= 0) {
			var end = source.lastIndexOf(boundaries[1]) + boundaries[1].length;
			return source.slice(0, start) + wrappedReplacement + source.slice(end);
		} else {
			return source + wrappedReplacement;
		}
	}

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(styleElement.styleSheet.cssText, index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(sourceMap && typeof btoa === "function") {
			try {
				css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(JSON.stringify(sourceMap)) + " */";
				css = "@import url(\"data:text/css;base64," + btoa(css) + "\")";
			} catch(e) {}
		}

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}


/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function() {
		var list = [];
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
		return list;
	}

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresListingsVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenresListingsVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, genreService) {
			var that = this;
			that.scopeNg = scopeNg;
			that.genreService = genreService;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {};
			that.scopeNg.datas.control = {};
			that.scopeNg.datas.external = {
				genres: that.getGenres()
			};
			that.scopeNg.datas.view = {
				mech: {
					pleaseWaitDisplayStyle: 'none',
					atrbListingsType: that.scopeNg.atrbListingsType
				},
				info: {
					myname: 'GenresListingsVcc',
					genres: that.scopeNg.datas.external.genres,
					listingsTitle: that.scopeNg.atrbListingsType
				}
			};
			console.log("GenresListings - genres : ", that.scopeNg.datas.view.info.genres);
		};

		p.getGenres = function getGenres() {	
			var that = this;
			var vv = {};
			vv.filters = "all";
			vv.userListingsType = "public";
			vv.useMockData = true;

			vv.getter =  that.genreService.getListings;
			vv.datas = vv.getter(
				{
					filters: vv.filters, 
					userListingsType: "public", 
					useMockData: true
				}
			);
			
			return vv.datas;
		};
		
		
		return CtrlClass;
		
	})();






/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsListingsVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationsListingsVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg, stationService, userService) {
			var that = this;
			that.scopeNg = scopeNg;
			that.stationService = stationService;
			that.userService = userService;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {};
			that.scopeNg.datas.control = {};
			that.scopeNg.datas.external = {
				stations: that.getStations(that.scopeNg.atrbListingsType)
			};
			that.scopeNg.datas.view = {
				mech: {
					pleaseWaitDisplayStyle: 'none',
					atrbListingsType: that.scopeNg.atrbListingsType
				},
				info: {
					myname: 'StationsListingsVcc',
					stations: that.scopeNg.datas.external.stations,
					listingsTitle: that.scopeNg.atrbListingsType
				}
			};
			console.log("StationsListings - stations : ", that.scopeNg.datas.view.info.stations);
		};

		p.getStations = function getStations(listingsType) {	
			var that = this;
			var vv = {};
			vv.filters = listingsType;
			vv.userListingsType = "public";
			vv.useMockData = true;
			
			if ( vv.filters === 'topStoriesStations' ) {
				vv.getter =  that.stationService.getListings;
			}
			else {
				vv.getter =  that.userService.getListings;
			}
			vv.datas = vv.getter(
				{
					filters: that.scopeNg.atrbListingsType, 
					userListingsType: "public", 
					useMockData: true
				}
			);
			
			return vv.datas;
		};
		
		
		return CtrlClass;
		
	})();






/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(33);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/GenresListingsVcc/GenresListingsVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/GenresListingsVcc/GenresListingsVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(27)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: GenresListingsVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-genres-listings-vcc {\n\tdisplay: block;\n\tpadding: 0px;\n\tcolor: #33a;\n\tborder: dotted 0px #33a;\n}\n\n\n.GenresListingsVcc { \n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33d;\n\tborder: dotted 1px #33d;\n}\n", ""]);

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: GenresListingsVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"GenresListingsVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<!-- <div>* {{datas.view.info.listingsTitle}}</div> -->  \n\t<ul>\n\t\t<li ng-repeat=\"Listing_ in datas.view.info.genres\"   ng-class=\"{ 'focsed' : Listing_.id == vv.FocsedItem.id}\">\n\t\t\t<a href=\"#/genre/{{Listing_.name}}/{{Listing_.id}}\"  data-vw-model-aspect={{Listing_}} data-listing-id={{Listing_.director}}  >\n\t\t\t\t{{Listing_.name}}\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n\n";

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(36);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/StationsListingsVcc/StationsListingsVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/StationsListingsVcc/StationsListingsVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(27)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: StationsListingsVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-stations-listings-vcc {\n\tdisplay: block;\n\tpadding: 0px;\n\tcolor: #33a;\n\tborder: dotted 0px #33a;\n}\n\n\n.StationsListingsVcc { \n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33d;\n\tborder: dotted 1px #33d;\n}\n", ""]);

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: StationsListingsVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\n<!-- This is the {{datas.view.info.myname}} Tmplt. -->\n<div  class=\"StationsListingsVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<!--- <div>* {{datas.view.info.listingsTitle}}</div> -->  \n\t<ul>\n\t\t<li ng-repeat=\"Listing_ in datas.view.info.stations\"   ng-class=\"{ 'focsed' : Listing_.id == vv.FocsedItem.id}\">\n\t\t\t<a href=\"#!/station/{{Listing_.name}}/{{Listing_.id}}\"  data-vw-model-aspect={{Listing_}} data-listing-id={{Listing_.director}}  >\n\t\t\t\t{{Listing_.name}}\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n\n";

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: GenreServiceDef.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

	//	INLINE: 
	//{
		var aa = {};
			
		//	Equated dependencies.
		//{
			///aa.baseServiceDefMp = require("../aa_baseModelServiceDef/aa_baseModelServiceDefFnc.js");
		//}

		aa.datasCache = {};

		return bgetPublicMethods
	//}



	//	PRIVATE METHODS:
	//{
		function bgetCacheKey(filters, usertype) {
			//	TODO
		}
		
		function TEMP_bgetMockData() {
			var datas = [
				{ name: 'Alternative', id: 'idalternative' },
				{ name: 'Blues', id: 'idblues' },
				{ name: 'Classical', id: 'idclassical' },
				{ name: 'Country', id: 'idcountry' },
				{ name: 'Easy Listening', id: 'ideasy' },
				{ name: 'Electronic/Dance', id: 'idelectronic' },
				{ name: 'Folk', id: 'idfolk' },
				{ name: 'Freeform', id: 'idfreeform' },
				{ name: 'Hip-Hop/Rap', id: 'ihip' },
				{ name: 'Inspirational', id: 'idinspirational' },
				{ name: 'Jazz', id: 'idjazz' },
				{ name: 'Latin', id: 'idlatin' },
				{ name: 'Metal', id: 'idmetal' },
				{ name: 'New Age', id: 'idnew' },
				{ name: 'Oldies', id: 'idoldies' },
				{ name: 'Pop', id: 'idpop' },
				{ name: 'R&B/Urban', id: 'idrb' },
				{ name: 'Reggae', id: 'idreggae' },
				{ name: 'Rock', id: 'idrock' },
				{ name: 'Seasonal/Holiday', id: 'iddeasonal' },
				{ name: 'Soundtracks', id: 'idsoundtracks' },
				{ name: 'Talk', id: 'idtalk' }
			];
			
			return datas;
		}
	//}


	//	PUBLIC METHODS:
	//{

		function bgetPublicMethods(injections) {
			var publicMethods = {};
			
			publicMethods.getListings = getListings;
			function getListings(api) {
				var vv = {}
				vv.injections = injections;
				vv.api = api || {}
				vv.filters = api.filters 			|| 'all'; 		/* array */
				vv.userType = api.userType 			|| 'public';	/* string */
				vv.useMockData = api.useMockData 	|| false;		/* boolean true to use mock data (that's provided) */
				vv.mockData = api.mockData  		|| null;		/* provide mock (JSON) data. */

				if ( vv.useMockData ) {
					vv.datas = TEMP_bgetMockData();
				}	

				return vv.datas;
			}
			
			return publicMethods;
		}
		//}

		
	})();



/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationServiceDef.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

	//	INLINE: 
	//{
		var aa = {};
			
		//	Equated dependencies.
		//{
			///aa.baseServiceDefMp = require("../aa_baseModelServiceDef/aa_baseModelServiceDefFnc.js");
		//}

		aa.datasCache = {};

		return bgetPublicMethods
	//}



	//	PRIVATE METHODS:
	//{
		function bgetCacheKey(filters, usertype) {
			//	TODO
		}
		
		function TEMP_bgetMockData(type) {
			var datas;
			if ( type === 'topStoriesStations' ) {
				datas = [
					{ name: 'Prince Best', id: 'idprince' },
					{ name: 'Beyonce Best', id: 'idbeyonce' },
					{ name: 'Kanye Best', id: 'idkanye' }
				];
			};
			return datas;
		}
	//}


	//	PUBLIC METHODS:
	//{

		function bgetPublicMethods(injections) {
			var publicMethods = {};
			
			publicMethods.getListings = getListings;
			function getListings(api) {
				var vv = {}
				vv.injections = injections;
				vv.api = api || {}
				vv.filters = api.filters 			|| 'all'; 		/* array */
				vv.userType = api.userType 			|| 'public';	/* string */
				vv.useMockData = api.useMockData 	|| false;		/* boolean true to use mock data (that's provided) */
				vv.mockData = api.mockData  		|| null;		/* provide mock (JSON) data. */

				if ( vv.useMockData ) {
					vv.datas = TEMP_bgetMockData( vv.filters.split()[0] );
				}	

				return vv.datas;
			}
			
			return publicMethods;
		}
		//}

		
	})();



/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: UserServiceDef.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

	//	INLINE: 
	//{
		var aa = {};
			
		//	Equated dependencies.
		//{
			///aa.baseServiceDefMp = require("../aa_baseModelServiceDef/aa_baseModelServiceDefFnc.js");
		//}

		aa.datasCache = {};

		return bgetPublicMethods
	//}



	//	PRIVATE METHODS:
	//{
		function bgetCacheKey(filters, usertype) {
			//	TODO
		}
		
		function TEMP_bgetMockData(type) {
			var datas;
			if ( type === 'recentStationsFromIdsListOfUser' ) {
				datas = [
						{ name: 'Stones Best', id: 'idstones' },
						{ name: 'Beatles Best', id: 'idbeatles' },
						{ name: 'Who Best', id: 'idwho' }
				];
			}
			else if ( type === 'favoriteStationsFromIdsListOfUser' ) {
				datas = [
					{ name: 'Beatles Best', id: 'idbeatles' },
					{ name: 'Zep Best', id: 'idzep' },
					{ name: 'Rush Best', id: 'idrush' }
				];
			};
			
			return datas;
		}
	//}


	//	PUBLIC METHODS:
	//{

		function bgetPublicMethods(injections) {
			var publicMethods = {};
			
			publicMethods.getListings = getListings;
			function getListings(api) {
				var vv = {}
				vv.injections = injections;
				vv.api = api || {}
				vv.filters = api.filters 			|| 'all'; 		/* array */
				vv.userType = api.userType 			|| 'public';	/* string */
				vv.useMockData = api.useMockData 	|| false;		/* boolean true to use mock data (that's provided) */
				vv.mockData = api.mockData  		|| null;		/* provide mock (JSON) data. */

				if ( vv.useMockData ) {
					vv.datas = TEMP_bgetMockData( vv.filters.split()[0] );
				}	

				return vv.datas;
			}
			
			return publicMethods;
		}
		//}

		
	})();



/***/ }
/******/ ])
