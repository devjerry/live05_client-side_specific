(function() {
	
    var app = angular.module('ChattyApp', []);
    var _events = {
        beforeStreamFirstTrack: 'beforeStreamFirstTrack',
        trackEnded: 'trackEnded',
        newTrackReady: 'newTrackReady',
        stopNow: 'stopNow'
    };
	console.log(">>> Chatty Inline");
	
	app.config(['$locationProvider', function appConfig($locationProvider) {


		// enable html5Mode for pushstate ('#'-less URLs)
		$locationProvider.html5Mode(true);
		$locationProvider.hashPrefix('!');

	}]);

	//##################################################################
    app.factory('socketSvc', ['$rootScope', function($rootScope) {
        var socket = io.connect('http://pi-node.nanocosm.com:3000/chat');  /*** NEEDS TO BE ABS ***/
        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {  
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },

            removeListener: function (eventName, callback) {
                socket.removeListener(eventName, callback);
            },

            emit: function (eventName, data, callback) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                });
            }
      };
    }]);
    
	//##################################################################
     app.factory('messagesSvc', ['socketSvc', '$timeout', function(socketSvc, $timeout) {
        var allMessages = [];
        var messagesSvc = {};

		
		function resizeViewport() {
			parent.document.getElementsByClassName("chatIframe")[0].style.height = window.document.body.scrollHeight + 'px';
		}

        function addMessage(messagePkg) {
            return allMessages.splice(0, 0, messagePkg);
        }

        socketSvc.on('radio:chat', function(data) {
			console.log(">>> Handle radio:chat.")
            if (data && data.messagePkg   &&  data.messagePkg.username   &&  data. messagePkg.message) {
                addMessage(data.messagePkg);
                $timeout(resizeViewport, 100);
            }
        });

        messagesSvc.sendTheMessage = function (channelId, messagePkg) {
            if ( channelId  &&  messagePkg  &&  messagePkg.username   &&   messagePkg.message ) {
				addMessage(messagePkg)

				socketSvc.emit('radio:chat', {
					stationName: channelId, 
					messagePkg: messagePkg
				});
				$timeout(resizeViewport, 100);

			}
        };

        messagesSvc.clearMessages = function() {
            allMessages = [];
        };

        messagesSvc.getAllTheMessages = function() {
            return allMessages;
        };

        return messagesSvc;

    }]);

	//##################################################################
    app.controller("chattingController", ['$scope', '$http', '$location', '$rootScope', 'socketSvc', 'messagesSvc', function($scope, $http, $location, $rootScope, socketSvc, messagesSvc) {
		
		console.log(">>> Chatty chattingController");

		//==============================================================
		//{
			var self = {};
			/// self.defaultChannelId = 48; 
			/*  Not presently used:
			self.public = {};
			self.public.applyChannel = function() {
				applyChannel();
			};
			*/

			$scope.data = {};
			$scope.data.newMessage = "";
			$scope.data.channelId = $location.search().channelId;
			$scope.data.username = $location.search().userDisplayName;
			
			socketSvc.emit('radio:ready');
			applyChannel($scope.data.channelId);


			
			//----------------------------------------------------------
			$scope.sendMessage = sendMessage;
			$scope.hasMessages = hasMessages;
			
			$scope.$on(_events.stopNow, function() {
				messagesSvc.clearMessages();
			});

			$scope.$on(_events.beforeStreamFirstTrack, function() {
				messagesSvc.clearMessages();
			});
			
			$scope.data.allMessages = getAllMessages();
        //}



		//==============================================================
		//{

			
			/* applyChannel needs to be hoisted */
			function applyChannel(channelId) {
				console.log(">>>> self: ", self)
				self.channelId = channelId;
				
				messagesSvc.clearMessages();
				///socketSvc.emit('radio:unchannel');
				///socketSvc.removeListener(/* 'connect', reconnect */);
				
				socketSvc.emit('radio:station', {stationName: getChannelId() });
				///socketSvc.on('connect', reconnect);
				
			};
			//{
				//	Helper:
					function reconnect() {
						socketSvc.emit( 'radio:station', {stationName: getChannelId() } );
					}
			//}

			function getChannelId() {
				return self.channelId;
			}

			
			/* Not used
			self.submit = function() {
				if (self.button === 'Play') {
					playChannel();
				} else {
					self.button = "Play";
					socketSvc.emit('radio:unchannel');
					socketSvc.removeListener('connect', reconnect);
					$rootScope.$broadcast(_events.stopNow);
				}
			};
			*/
			
			//----------------------------------------------------------
			function sendMessage() {
				console.log(">>>sendMessage - self.channelId, $scope.data.newMessage: ", self.channelId, $scope.data.newMessage);
				
				var messagePkg = {
					username: $scope.data.username,
					timePosted: Date.now(),
					message: $scope.data.newMessage
				};
				
				messagesSvc.sendTheMessage(self.channelId, messagePkg);
				$scope.data.newMessage = "";
			};
			
			function hasMessages() {
				if (messagesSvc.getAllTheMessages().length) {
					return true;
				}
				return false;
			};

			function getAllMessages() {
				return messagesSvc.getAllTheMessages();
			};


		//}
        

    
    }]);


})();
