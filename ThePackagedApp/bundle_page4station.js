"use strict";


/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4StationDtlVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(7);
			__webpack_require__(1);
			__webpack_require__(2);
			__webpack_require__(3);
			__webpack_require__(4);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(9);
			aa.LinkFncClass = __webpack_require__(5);
			aa.CtrlClass = __webpack_require__(6);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'Page4StationDtlVcc';
			aa.thisMod = aa.Page4StationDtlVcc = angular.module(aa.thisModsName, [
				'HierarchyNavStripVcc',
				'StationInfoPanelVcc',
				'DjProfilePanelVcc',
				'LastPlayedPanelVcc'
			]);
			
			aa.drctName = 'drctPage4StationDtlVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: HierarchyNavStripVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(19);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(21);
			aa.LinkFncClass = __webpack_require__(10);
			aa.CtrlClass = __webpack_require__(11);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'HierarchyNavStripVcc';
			aa.thisMod = aa.HierarchyNavStripVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctHierarchyNavStripVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationInfoPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			__webpack_require__(22);
			__webpack_require__(12);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(24);
			aa.LinkFncClass = __webpack_require__(13);
			aa.CtrlClass = __webpack_require__(14);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'StationInfoPanelVcc';
			aa.thisMod = aa.StationInfoPanelVcc = angular.module(aa.thisModsName, [
				'SocialingPanelVcc'
			]);
			
			aa.drctName = 'drctStationInfoPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: DjProfilePanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(25);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(27);
			aa.LinkFncClass = __webpack_require__(15);
			aa.CtrlClass = __webpack_require__(16);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'DjProfilePanelVcc';
			aa.thisMod = aa.DjProfilePanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctDjProfilePanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: LastPlayedPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(28);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(30);
			aa.LinkFncClass = __webpack_require__(17);
			aa.CtrlClass = __webpack_require__(18);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'LastPlayedPanelVcc';
			aa.thisMod = aa.LastPlayedPanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctLastPlayedPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4StationDtlVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: Page4StationDtlVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'Page4StationDtlVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(8);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(31)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/Page4StationDtlVcc/Page4StationDtlVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/Page4StationDtlVcc/Page4StationDtlVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(32)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: Page4StationDtlVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-page-4-station-dtl-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33a;\n\tborder: dotted 1px #33a;\n}\n\n.Page4StationDtlVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: solid 1px #33f;\n}\n", ""]);

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: Page4StationDtlVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\nThis is the {{datas.view.info.myname}} Tmplt.\n<div  class=\"Page4StationDtlVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<drct-hierarchy-nav-strip-vcc></drct-hierarchy-nav-strip-vcc>\n\t<drct-station-info-panel-vcc></drct-station-info-panel-vcc>\n\t<drct-dj-profile-panel-vcc></drct-dj-profile-panel-vcc>\n\t<drct-last-played-panel-vcc></drct-last-played-panel-vcc>\n\t<a href=\"#!/home\">Go to Home Page</a>\n</div>\nStationInfoPanelVcc\n\n\n\n";

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: HierarchyNavStripVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: HierarchyNavStripVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'HierarchyNavStripVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: SocialingPanelVcc.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports
	 
	 
	//	INLINE: 
	//{
		var aa = {};
		
		//	Non-equated dependencies.
		//{
			aa.style = __webpack_require__(35);
		//}
		
		//	Equated dependencies.
		//{
			aa.tmplt = __webpack_require__(37);
			aa.LinkFncClass = __webpack_require__(33);
			aa.CtrlClass = __webpack_require__(34);
		//}
		
		
		//	Create module and its directive:
		//{
			aa.thisModsName = 'SocialingPanelVcc';
			aa.thisMod = aa.SocialingPanelVcc = angular.module(aa.thisModsName, [
			]);
			
			aa.drctName = 'drctSocialingPanelVcc';
			createDrct(aa.thisMod, aa.drctName);
		//}
	//}


	//	HELPERS TO BUILD MODULE'S DIRECTIVE:
	//{

		function createDrct(module, drctName) {
			module.directive(drctName, ['$compile', drctFunction]);	
		}
		
		function drctFunction($compile) {
			var vv = {}
			
			// Get parts.
			//{
				vv.tmplt = aa.tmplt; 
				vv.LinkFncClass = aa.LinkFncClass;
				vv.CtrlClass = aa.CtrlClass;
			//}
			
			//	Setup the linkFnc.
			vv.linkFnc = function(scopeNgg, elm, attr) { 
				new vv.LinkFncClass(scopeNgg, elm, attr);
			}
			//	Define injection into the Ctrl class.
			vv.controller = ['$scope', vv.CtrlClass];
			
			//	Return the directive's definition.
			return  {
				scope: {},
				restrict: 'E',
				template: vv.tmplt,
				link: vv.linkFnc,
				controller: vv.controller
			};
		};	
	//}	



/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationInfoPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: StationInfoPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'StationInfoPanelVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: DjProfilePanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: DjProfilePanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'DjProfilePanelVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: LastPlayedPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: LastPlayedPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'LastPlayedPanelVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(20);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(31)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/HierarchyNavStripVcc/HierarchyNavStripVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/HierarchyNavStripVcc/HierarchyNavStripVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(32)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: HierarchyNavStripVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-hierarchy-nav-strip-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33a;\n\tborder: dotted 1px #33a;\n}\n\n.HierarchyNavStripVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: solid 1px #33f;\n}\n", ""]);

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: HierarchyNavStripVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\nThis is the {{datas.view.info.myname}} Tmplt.\n<div  class=\"HierarchyNavStripVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n</div>\n\n";

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(23);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(31)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/StationInfoPanelVcc/StationInfoPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/StationInfoPanelVcc/StationInfoPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(32)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: StationInfoPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-station-info-panel-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33a;\n\tborder: dotted 1px #33a;\n}\n\n.StationInfoPanelVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: solid 1px #33f;\n}\n", ""]);

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: StationInfoPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\nThis is the {{datas.view.info.myname}} Tmplt.\n<div  class=\"StationInfoPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n\t<drct-socialing-panel-vcc></drct-socialing-panel-vcc>\n</div>\n\n";

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(26);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(31)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/DjProfilePanelVcc/DjProfilePanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/DjProfilePanelVcc/DjProfilePanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(32)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: DjProfilePanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-dj-profile-panel-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33a;\n\tborder: dotted 1px #33a;\n}\n\n.DjProfilePanelVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: solid 1px #33f;\n}\n", ""]);

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: DjProfilePanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\nThis is the {{datas.view.info.myname}} Tmplt.\n<div  class=\"DjProfilePanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n</div>\n\n";

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(29);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(31)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/LastPlayedPanelVcc/LastPlayedPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/LastPlayedPanelVcc/LastPlayedPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(32)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: LastPlayedPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-last-played-panel-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33a;\n\tborder: dotted 1px #33a;\n}\n\n.LastPlayedPanelVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: solid 1px #33f;\n}\n", ""]);

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: LastPlayedPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\nThis is the {{datas.view.info.myname}} Tmplt.\n<div  class=\"LastPlayedPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n</div>\n\n";

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isIE9 = memoize(function() {
			return /msie 9\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0;

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isIE9();

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function createStyleElement() {
		var styleElement = document.createElement("style");
		var head = getHeadElement();
		styleElement.type = "text/css";
		head.appendChild(styleElement);
		return styleElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement());
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else {
			styleElement = createStyleElement();
			update = applyToTag.bind(null, styleElement);
			remove = function () {
				styleElement.parentNode.removeChild(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	function replaceText(source, id, replacement) {
		var boundaries = ["/** >>" + id + " **/", "/** " + id + "<< **/"];
		var start = source.lastIndexOf(boundaries[0]);
		var wrappedReplacement = replacement
			? (boundaries[0] + replacement + boundaries[1])
			: "";
		if (source.lastIndexOf(boundaries[0]) >= 0) {
			var end = source.lastIndexOf(boundaries[1]) + boundaries[1].length;
			return source.slice(0, start) + wrappedReplacement + source.slice(end);
		} else {
			return source + wrappedReplacement;
		}
	}

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(styleElement.styleSheet.cssText, index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(sourceMap && typeof btoa === "function") {
			try {
				css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(JSON.stringify(sourceMap)) + " */";
				css = "@import url(\"data:text/css;base64," + btoa(css) + "\")";
			} catch(e) {}
		}

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}


/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function() {
		var list = [];
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
		return list;
	}

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: SocialingPanelVccLinkFncClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {
	 
		function LinkFncClass(scopeNg, elm, attr) {
			var that = this;
			that.scopeNg = scopeNg;
			that.elm = elm;
			that.attr = attr;
			that.setup();
		};
			
		var p = LinkFncClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setClickHandler();
		};
		
		p.setClickHandler = function setClickHandler() {
			var that = this;
			that.elm.on( 
				{ 
					'click': function handleClick_fnc(Event) {
						///alert("BOOOOOOOO to " + that.scopeNg.datas.view.info.name);
					}
				}
			)
		};
		
		return LinkFncClass;
		
	})();



/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	/*
	 * *********************************************************************
	 * *********************************************************************
	 * *********************************************************************
	 * File: SocialingPanelVccCtrlClass.js
	 * *********************************************************************
	 * *********************************************************************
	 */
	module.exports = (function() {

		function CtrlClass(scopeNg) {
			var that = this;
			that.scopeNg = scopeNg;
			that.setup()
		}
			
		var p = CtrlClass.prototype;
		
		p.setup = function setup() {
			var that = this;
			that.setupDatas();
		};
		
		p.setupDatas = function setupDatas() {
			var that = this;
			that.scopeNg.datas = {
				control: {},
				external: {},
				view: {
					mech: {
						pleaseWaitDisplayStyle: 'none'
					},
					info: {
						myname: 'SocialingPanelVcc'
					}
				}
			};
		};

		return CtrlClass;
		
	})();





/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(36);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(31)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/SocialingPanelVcc/SocialingPanelVccLinkStyle.css", function() {
			var newContent = require("!!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/node_modules/css-loader/index.js!/home/webjerry/MyAliases/CHOPPS-ALIAS/J-CO-ENTITY/HttpSystems/Websites/JerryzLiveO5Site/client_side/specific_REPO/env_angular/vcomp_sys_extended/implementation/vcomps/vcomps_concrete_by_page/SocialingPanelVcc/SocialingPanelVccLinkStyle.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(32)();
	exports.push([module.id, "/*\n * *********************************************************************\n * *********************************************************************\n * *********************************************************************\n * File: SocialingPanelVccLinkStyle.css\n * *********************************************************************\n * *********************************************************************\n */\n \ndrct-socialing-panel-vcc {\n\tdisplay: block;\n\tpadding: 5px;\n\tcolor: #33a;\n\tborder: dotted 1px #33a;\n}\n\n.SocialingPanelVcc {\n\tpadding: 5px;\n\tcolor: #33f;\n\tborder: solid 1px #33f;\n}\n", ""]);

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<!-- =============================================================== -->\n<!-- =============================================================== -->\n<!-- File: SocialingPanelVccLinkTmplt.html -->\n<!-- =============================================================== -->\n\nThis is the {{datas.view.info.myname}} Tmplt.\n<div  class=\"SocialingPanelVcc\">\n\tThis is the inside of {{datas.view.info.myname}}.\n</div>\n\n";

/***/ }
/******/ ])
